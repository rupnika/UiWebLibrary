# UiWebLibrary - Mark <!-- omit in toc -->

[Angular Material](https://material.angular.io/components) components wrapper library.

🔈 [UiWebLibrary DEMO](https://rupnika.gitlab.io/uiweblibrarydemo/)

## Table of contents <!-- omit in toc -->

-   [🚩 How to use](#-how-to-use)
    -   [Angular project structure](#angular-project-structure)
    -   [Import `UiWebLibrary`](#import-uiweblibrary)
    -   [Install NPM packages](#install-npm-packages)
    -   [Styling](#styling)
    -   [Localization](#localization)
-   [🚩 Available components](#-available-components)
    -   [✔️ Table](#️-table)
    -   [✔️ Dropdown/Autocomplete](#️-dropdownautocomplete)
    -   [✔️ Button](#️-button)
    -   [✔️ Dropdown button](#️-dropdown-button)
    -   [✔️ Card](#️-card)
    -   [✔️ Dialog](#️-dialog)
    -   [✔️ Menu](#️-menu)
    -   [✔️ Dual list](#️-dual-list)

## 🚩 How to use

### Angular project structure

In your Angular application folder, create `/shared` folder. This will be base folder for `UiWebLibrary` configuration. Also create `/shared/styles` folder (will be used later).
You should have following structure (`my-project` is root of your project/gitlab repository).

![dfd](assets/images/struct.png)

Exact location/name of `/Angular` folder can be changed, as long as you have `/shared` and `/shared/styles` folders inside.

### Import `UiWebLibrary`

Add `UiWebLibrary` as a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). Open/create `my-project/.gitmodules` file and add:

```
[submodule "UiWebLibrary"]
    path = src/Angular/shared/UiWebLibrary
    url = https://gitlab.com/rupnika/UiWebLibrary.git
    branch = release/formnet2
```

> To initialize or update `UiWebLibrary` source code, use following commands:
>
> ```bash
> # Clone `UiWebLibrary` as submodule
> git submodule sync --recursive
> git submodule update --init --recursive --remote
> # Update `UiWebLibrary` submodule project
> git submodule update --remote
> ```

### Install NPM packages

`UiWebLibrary` needs following NPM packages installed and added to `src/Angular/package.json`.

```
npm install dayjs file-saver guid-typescript ngx-highlightjs --save
```

### Styling

Default styling for `UiWebLibrary` is defined in `src/Angular/shared/UiWebLibrary/assets/styles` folder. To override default styles, create files in `src/Angular/shared/styles` folder.

![dfd](assets/images/styles.png)

### Localization

`UiWebLibrary` supports localization of components to `English`, `German` and `Slovenian` language. To provide default language of library components, inject `UI_LANG` token into your main module.

```typescript
@NgModule({
	declarations: [],
	imports: [],
	providers: []
	exports: [],
})
export class AppModule {
	/** All shared services should be defined here @IMPORTANT !!! */
	static forRoot(): ModuleWithProviders<AppModule> {
		return {
			ngModule: AppModule,
			providers: [
				{ provide: UI_LANG, useValue: UiLanguage.English }, // <==== Set initial language here
			],
		};
	}
}
```

## 🚩 Available components

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Table

---

#### HTML <!-- omit in toc -->

```html
<ui-table
	[uiTableConfig]="tableConfig"
	(onRowDoubleClick)="onRowDblClick($event)"
	(onRowClick)="onRowClick($event)"
></ui-table>
```

#### Typescript <!-- omit in toc -->

```typescript
tableConfig: UiTableConiguration<T>;    // replace T with your data type

// Example of minimum configuration. A lot more configuration options are available
private initTable() {
    this.tableConfig = new UiTableConiguration<T>();  // replace T with your data type
    this.tableConfig.paginator.pageSize = 10;
    this.tableConfig.paginator.showPaginator = true;
    this.tableConfig.table.isExportVisible = false;
    this.tableConfig.alternatingColor = true; // if you want alternating color
    // Define table columns and data
    this.tableConfig.columns = [
        {
            fieldName: 'id',
            headerText: 'Ident',
            cellDataType: UiTableCellDataType.Text, // Text is default data type. Don't need to explicitly specify!
            // Enable inline editing mode for this cell. Apply only to cellDataType: UiTableCellDataType.Text. Default is FALSE.
            inlineEditEnabled: () => {},
            onInlineEditFinished: (newValue: string, row: T, origRow: K) => {
					console.log('NEW VALUE', newValue, row);
				},
        },
        {
            fieldName: 'name',
            headerText: 'Column 2',
            cellDataType: UiTableCellDataType.Date,
        },
        // Define table dropdown
        {
			{
				fieldName: 'fieldName',
				cellDataType: UiTableCellDataType.Dropdown,
				filterEnabled: false,
				sortingEnabled: false,
				headerText: 'Column3',
			},
        },
        {
            fieldName: 'Icons',
            cellDataType: UiTableCellDataType.Icon,
            filterEnabled: false,
            sortingEnabled: false,
            cellIcons: [
                {
                    isRouterLink: true,
                    isVisible: (el: T) => {   // replace T with your type
                        return true;
                    },
                    getTooltipText: (el: T) => {   // replace T with your type
                        return 'tooltip text';
                    },
                    getIcon: (el: T) => {   // replace T with your type
                        return this.resSvc.falTrashAlt;
                    },
                    getIconClass: (el: T) => {   // replace T with your type
                        return 'icon-red';
                    },
                    onClick: (el: T) => {   // replace T with your type
                        this.onIconXClick(el);
                    },
                },
            ],
        },
    ];
}
```

#### Show data <!-- omit in toc -->

```typescript
this.tableConfig.initTable(T[]); // replace T with your data array
```

#### Show dropdown <!-- omit in toc -->

```typescript
this.tableConfig.columns.forEach((col: UiTableColumn) => {
	col.dropdown = {
		data: any[],
		idField: 'idField',
		valueField: 'valueField',
	};
});
```

#### Events <!-- omit in toc -->

```typescript
onRowDoubleClick(row: T) { }
onRowClick(row: T) { }
```

#### Special fields <!-- omit in toc -->

To programatically define preselected rows in table, you can use selectChecboxes(rows: [], isChild?: boolean) method.

```typescript
let preselected: any[] = [];
this.tableConfig.dataSource.data.forEach((row) => {
	preselected.push(row);
});
this.tableConfig.selectChecboxes(preselected);
```

To programatically define selected rows in table, you can use `TABLE_IS_SELECTED` field.

```typescript
T[].forEach((x: T) => {
	x[TABLE_IS_SELECTED] = true|false;
});
```

If you want for selected table row to be permanently selected, use `TABLE_IS_SELECTED_PERMA` field.

```typescript
T[].forEach((x: T) => {
	x[TABLE_IS_SELECTED_PERMA] = true|false;
});
```

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Dropdown/Autocomplete

---

#### HTML `dropdown` <!-- omit in toc -->

```html
<ui-form-input
	inputType="dropdown"
	formCtrlName=""
	[myForm]="myForm"
	[dropdownConfig]="dropdownConfig"
	(onDropdownSelected)="onDropdownSelected($event)"
	(onDropdownCleared)="onDropdownCleared()"
	(onDropdownNewEntry)="onDropdownNewEntry($event)"
>
</ui-form-input>
```

#### Typescript <!-- omit in toc -->

Replace `T` with your data type.

```typescript
dropdownConfig: UiDropdownConfig<T>; // create component configuration property

constructor() {
    this.dropdownConfig = new UiDropdownConfig<T>();
}
```

When data for dropdown is prepared, create dropdown/autocomplete configuration.

```typescript
someMethod() {
    this.dropdownConfig.label = '';                  // label to be shown above autocomplete input
    this.dropdownConfig.allowNewEntry = true;        // allow user to create new entry from current input text. Only for autocomplete.
    this.dropdownConfig.data = T[];                  // array with your data of type T
    this.dropdownConfig.autocomplete = true;
    this.dropdownConfig.initDropdown('Id', 'Title'); // populate dropdown with data
}
```

> To populate dropdown/autocomplete control with your data, call `initDropdown()` method that accepts two parameters:
>
> -   id - name of the field from your data array `T[]` used for dropdown id value. **If not specified, it will be autogenerated.**
> -   display - name of the field from your data array `T[]` used for dropdown display value
>
> To ensure type safety when providing `id`/`display` field names, you can use following code:
>
> ```typescript
> this.dropdownConfig.initDropdown(nameof<T>('Id'), nameof<T>('Title'));
> ```
>
> Define `nameof` somewhere in your component class.
>
> ```typescript
> const nameof = <T>(name: keyof T) => name;
> ```

#### Events <!-- omit in toc -->

Replace `T` with your data type.

```typescript
// Dropdown option selected
onDropdownSelected(data: T) {

}
// No dropdown option selected
onDropdownCleared(data: T) {

}
// Only if set
// this.dropdownConfig.allowNewEntry = true
onDropdownNewEntry(entry: UiDropdownEntry) {

}

//dropDown showing icon next item - example with some data list in dropdown
//DROPDOWN_SHOW_ICON constant - to tell drodown if icon will be shown or not
//DROPDOWN_ICON constant - define which icon to show from resources(IconDefinition)
if (true) {
    this.T[].forEach((t) => {
        if (t === <value>) {
            t[DROPDOWN_SHOW_ICON] = true;
            t[DROPDOWN_ICON] = this.resSvc.falCheck;
        }
    });
    ////AND THEN populate dropdown with data!!!
    this.targetEntityDropdownConfig.initDropdown(
        nameof<T>('Id'),
        nameof<T>('Title')
    );
}
```

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Input

#### 1.) Text

#### 2.) Numeric

#### 3.) Checkbox

#### 4.) Datetime

#### 5.) File select

#### 6.) File button select

```html
<ui-form-input
	inputType="fileButton"
	[fileButtonIcon]=""
	[allowedExtensions]="allowedExtensions"
	multipleSelect="multiple"
	(onFileSelected)="onFileSelected($event)"
></ui-form-input>
```

```typescript
allowedExtensions: string[] = ['.dll', '...'];
files: FileHolder[] = [];

onFileSelected(fileHolderObv: Observable<FileHolder>): void {
    fileHolderObv.subscribe(
        (value: FileHolder) => {
            this.files.push(value);
        },
        (err) => { },
        () => {
            // all files loaded
        }
    );
}
```

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Button

---

#### HTML <!-- omit in toc -->

Example:

```html
<ui-button text="Click me" (onClick)="onClick()"></ui-button>
```

Example with all properties:

```html
<ui-button
	buttonType="stroked"
	text="Click me"
	(onClick)="onClick()"
	[faIcon]=""
	[isDisabled]="!myForm.valid"
	[isVisible]="true"
	[disableOnApiRequest]="true"
	cssButtonClass=""
></ui-button>
```

Available `buttonType` values:

-   `flat` (default)
-   `basic`
-   `raised`
-   `stroked`

Available `buttonColor` values:

-   `primary` (default)
-   `secondary`
-   `warn`
-   `link`

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Dropdown button

---

#### HTML <!-- omit in toc -->

🚧 TODO

#### Typescript <!-- omit in toc -->

🚧 TODO

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Card

#### HTML <!-- omit in toc -->

```html
<ng-container buttons> ..... </ng-container>
```

🚧 TODO

#### Typescript <!-- omit in toc -->

🚧 TODO

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Dialog

---

#### HTML <!-- omit in toc -->

🚧 TODO

#### Typescript <!-- omit in toc -->

🚧 TODO

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Menu

---

#### HTML <!-- omit in toc -->

🚧 TODO

#### Typescript <!-- omit in toc -->

🚧 TODO

<!-- ================================================================================ -->
<!-- ================================================================================ -->

### ✔️ Dual list

```typescript
//showMoveAllLeft - for hiding button for moving all left - default is true
//showMoveAllRight - for hiding button for moving all left - default is true
```

```html
<ui-dual-list [showMoveAllLeft]="false" [showMoveAllRight]="false"></ui-dual-list>
```

---

#### HTML <!-- omit in toc -->

🚧 TODO

#### Typescript <!-- omit in toc -->

🚧 TODO
