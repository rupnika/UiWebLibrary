import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewEncapsulation,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { UiButtonColor, UiButtonType } from 'shared/UiWebLibrary/entities/ui-controls';
import { UiIconService } from 'shared/UiWebLibrary/services/ui-icon.service';
import { UiTextService } from 'shared/UiWebLibrary/services/ui-text.service';
@Component({
	selector: 'ui-dialog-content',
	templateUrl: './ui-dialog-content.component.html',
	styleUrls: ['./ui-dialog-content.component.scss'],
	encapsulation: ViewEncapsulation.None,
	// host: { class: 'ui-dialog-content' },
})
export class UiDialogContentComponent implements OnInit {
	@Input() title: string;
	@Input() cssClass: string;
	@Input() cssClassContent: string;
	@Input() myForm: FormGroup = new FormGroup({});
	@Input() hideButtons: boolean = true;

	@Input() hasStickyHeader: boolean = true;
	@Input() isDialogContentScrollable: boolean = true;

	// OUT
	@Output() onSaveClick: EventEmitter<any> = new EventEmitter();
	@Output() onCloseClick: EventEmitter<any> = new EventEmitter();

	@Input() showStandardButtons: boolean = false;
	@Input() showButtonsInHeader: boolean = false;

	/** If 'showStandardButtons' enabled. */
	@Input() showSaveButton: boolean = true;
	@Input() saveButtonOrder: number = 0; // 0|1
	@Input() saveButtonType: UiButtonType = 'flat';
	@Input() saveButtonColor: UiButtonColor = 'primary';
	@Input() saveButtonText: string = '';
	@Input() saveButtonIcon: any;

	@Input() saveButtonTooltip: string = '';
	@Input() saveButtonId: string = '';

	/** If 'showStandardButtons' enabled. */
	@Input() showCloseButton: boolean = true;
	@Input() closeButtonType: UiButtonType = 'flat';
	@Input() closeButtonColor: UiButtonColor = 'warn';
	@Input() closeButtonText: string = '';
	@Input() closeButtonIcon: any;
	@Input() closeButtonId: string = '';

	public hasStickyTitle: boolean = false;
	constructor(
		public el: ElementRef,
		public ren: Renderer2,
		public iconService: UiIconService,
		public textSvc: UiTextService
	) {
		this.saveButtonText = this.textSvc.getText('save');
		this.closeButtonText = this.textSvc.getText('cancel');
		this.saveButtonIcon = this.iconService.getIconByKey('falSave');
	}

	ngOnInit(): void {
		this.isDialogContentScrollable = _.isNil(this.isDialogContentScrollable)
			? true
			: this.isDialogContentScrollable;
		this.hasStickyHeader = _.isNil(this.hasStickyHeader) ? true : this.hasStickyHeader;

		this.ren.addClass(this.el.nativeElement, 'ui-dialog-content');

		if (!this.hasStickyHeader) {
			this.ren.addClass(this.el.nativeElement, 'ui-dialog-content-scrollable');
		} else {
			this.ren.addClass(this.el.nativeElement.parentElement.parentElement, 'hidden-overflow');
		}

		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
		console.log(this.hasStickyHeader);
		this.hasStickyTitle = this.hasStickyHeader;
	}

	_onSaveClicked(): void {
		this.onSaveClick.emit();
	}
	_onCloseClicked(): void {
		this.onCloseClick.emit();
	}

	getDialogContentClasses() {
		return `${this.cssClassContent} ${
			this.isDialogContentScrollable ? 'ui-dialog-content-scrollable' : 'hidden-overflow'
		}`;
	}
}
