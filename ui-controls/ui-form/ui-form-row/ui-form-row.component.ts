import { Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'ui-form-row',
	templateUrl: './ui-form-row.component.html',
	styleUrls: ['./ui-form-row.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class UiFormRowComponent implements OnInit {
	@Input() numOfElements: number = 1;
	@Input() cssClass: string;
	@Input() justifyContent: boolean = true;

	constructor(public el: ElementRef, public ren: Renderer2) {}

	ngOnInit(): void {}

	ngAfterViewInit() {
		this.ren.addClass(this.el.nativeElement, 'ui-form-row');
		if (!this.justifyContent) {
			this.ren.addClass(this.el.nativeElement, 'no-justify');
		}

		if (this.numOfElements === 1) {
			this.ren.addClass(this.el.nativeElement, 'ui-row-1');
		} else if (this.numOfElements === 2) {
			this.ren.addClass(this.el.nativeElement, 'ui-row-2');
		} else if (this.numOfElements === 3) {
			this.ren.addClass(this.el.nativeElement, 'ui-row-3');
		} else if (this.numOfElements === 4) {
			this.ren.addClass(this.el.nativeElement, 'ui-row-4');
		}

		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}
}
