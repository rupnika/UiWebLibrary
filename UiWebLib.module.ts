import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule, registerLocaleData } from '@angular/common';
import localeSl from '@angular/common/locales/en-DE';
import { LOCALE_ID, ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OAuthService } from 'angular-oauth2-oidc';
import { HighlightModule } from 'ngx-highlightjs';
import { PendingRequestsInterceptorProvider } from './http-spinner/services/pending-requests-interceptor.service';
import { UiDatePipe, UiDateTimePipe, UiTimePipe } from './pipes/dateTime.pipe';
import { UiLocalDatePipe, UiLocalDateTimePipe, UiLocalTimePipe } from './pipes/localDateTime.pipe';
import { UiSafeHtmlPipe } from './pipes/safeHtml.pipe';
import { UiSafeImagePipe } from './pipes/safeImage.pipe';
import { UiSafeUrlPipe } from './pipes/safeUrl.pipe';
import { UiGbsOAuthService } from './services/gbs-oauth.service';
import { UiTextService } from './services/ui-text.service';
import { UiButtonComponent } from './ui-controls/ui-button/ui-button.component';
import { UiCardComponent } from './ui-controls/ui-card/ui-card.component';
import { UiCodeComponent } from './ui-controls/ui-code/ui-code.component';
import { UiConfirmDialogComponent } from './ui-controls/ui-dialog/ui-confirm-dialog/ui-confirm-dialog.component';
import { UiDialogButtonsComponent } from './ui-controls/ui-dialog/ui-dialog-buttons/ui-dialog-buttons.component';
import { UiDialogContentComponent } from './ui-controls/ui-dialog/ui-dialog-content/ui-dialog-content.component';
import { UiNotifyDialogComponent } from './ui-controls/ui-dialog/ui-notify-dialog/ui-notify-dialog.component';
import { UiDropdownItemComponent } from './ui-controls/ui-dropdown-menu/ui-dropdown-item/ui-dropdown-item.component';
import { UiDropdownMenuComponent } from './ui-controls/ui-dropdown-menu/ui-dropdown-menu/ui-dropdown-menu.component';
import { UiDualListComponent } from './ui-controls/ui-dual-list/ui-dual-list.component';
import { UiFormInputComponent } from './ui-controls/ui-form/ui-form-input/ui-form-input.component';
import { UiFormLabelComponent } from './ui-controls/ui-form/ui-form-label/ui-form-label.component';
import { UiFormRowComponent } from './ui-controls/ui-form/ui-form-row/ui-form-row.component';
import { UiGlobalSpinnerComponent } from './ui-controls/ui-globalspinner/globalspinner.component';
import { UiIconPairComponent } from './ui-controls/ui-icon-pair/ui-icon-pair.component';
import { UiMenuComponent } from './ui-controls/ui-menu/ui-menu.component';
import { UiSnackBarComponent } from './ui-controls/ui-snack-bar/ui-snack-bar.component';
import { FilterTemplateComponent } from './ui-controls/ui-table/filter-template/filter-template.component';
import { getPaginatorTranslations } from './ui-controls/ui-table/paginator-translations';
import { UiTableComponent } from './ui-controls/ui-table/ui-table.component';

registerLocaleData(localeSl);

@NgModule({
	declarations: [
		UiLocalDateTimePipe,
		UiLocalDatePipe,
		UiLocalTimePipe,
		UiDateTimePipe,
		UiDatePipe,
		UiTimePipe,
		UiSafeHtmlPipe,
		UiSafeImagePipe,
		UiSafeUrlPipe,
		UiGlobalSpinnerComponent,
		UiSnackBarComponent,
		UiTableComponent,
		UiSnackBarComponent,
		UiConfirmDialogComponent,
		UiNotifyDialogComponent,
		UiButtonComponent,
		FilterTemplateComponent,
		UiFormInputComponent,
		UiFormRowComponent,
		UiDialogContentComponent,
		UiDialogButtonsComponent,
		UiCardComponent,
		UiFormLabelComponent,
		UiMenuComponent,
		UiDropdownMenuComponent,
		UiDropdownItemComponent,
		UiDualListComponent,
		UiCodeComponent,
		UiIconPairComponent,
	],
	imports: [
		CommonModule,
		CdkTableModule,
		FontAwesomeModule,
		MatProgressSpinnerModule,
		MatTableModule,
		MatPaginatorModule,
		MatTooltipModule,
		MatDialogModule,
		MatButtonModule,
		MatInputModule,
		MatMenuModule,
		MatSortModule,
		MatSelectModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatCheckboxModule,
		MatRadioModule,
		MatAutocompleteModule,
		FormsModule,
		ReactiveFormsModule,
		HighlightModule,
	],
	providers: [
		OAuthService,
		UiGbsOAuthService,
		{ provide: MatPaginatorIntl, useValue: getPaginatorTranslations() },
		{ provide: LOCALE_ID, useValue: 'en-DE' },
		{ provide: MAT_DATE_LOCALE, useValue: 'en-DE' },
	],
	exports: [
		//UiGbsOAuthService,
		UiDateTimePipe,
		UiDatePipe,
		UiTimePipe,
		UiSafeHtmlPipe,
		UiSafeImagePipe,
		UiGlobalSpinnerComponent,
		UiTableComponent,
		UiSnackBarComponent,
		UiConfirmDialogComponent,
		UiNotifyDialogComponent,
		UiButtonComponent,
		UiFormInputComponent,
		UiFormRowComponent,
		UiDialogContentComponent,
		UiDialogButtonsComponent,
		UiCardComponent,
		UiFormLabelComponent,
		UiMenuComponent,
		MatSelectModule,
		MatAutocompleteModule,
		UiDropdownMenuComponent,
		UiDropdownItemComponent,
		UiDualListComponent,
		UiCodeComponent,
		UiIconPairComponent,
	],
})
export class UiWebLibModule {
	public static forRoot(): ModuleWithProviders<UiWebLibModule> {
		return {
			ngModule: UiWebLibModule,
			providers: [
				PendingRequestsInterceptorProvider,
				UiGbsOAuthService,
				UiTextService,
				UiDatePipe,
				UiDateTimePipe,
				UiSafeImagePipe,
			],
		};
	}
}
