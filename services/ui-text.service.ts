import { Inject, Injectable, Optional } from '@angular/core';
import * as _ from 'lodash';
import * as textData from '../assets/textinfo.json';
import { UiLanguage } from '../entities/enums';
import { UI_DIGITS_DEFAULT, UI_DIGITS_SEPARATOR, UI_LANG } from './../entities/misc';

@Injectable({
	providedIn: 'root',
})
export class UiTextService {
	/**
	 * Content of text file
	 */
	textData: any;

	constructor(
		@Optional() @Inject(UI_DIGITS_DEFAULT) public defaultDigits: number,
		@Optional() @Inject(UI_DIGITS_SEPARATOR) public digitsSeparator: string,
		@Inject(UI_LANG) public uiLang: UiLanguage
	) {
		if (_.isNil(defaultDigits)) {
			this.defaultDigits = 2;
		}
		if (_.isNil(digitsSeparator)) {
			this.digitsSeparator = ',';
		}
		this.textData = textData;
	}

	public getText(name: string, optional?: string[]): string {
		let text = '';

		// get text by key
		const row = this.textData.default[name];

		// key doesn't exist
		if (!row) {
			return `MISSING_KEY: '${name}'`;
		}

		// select language from the text row
		text = _.isNil(this.uiLang) ? row[UiLanguage.English] : row[this.uiLang];
		if (optional) {
			optional.forEach((e, i) => {
				text = text.replace(`{${i}}`, e);
			});
		}

		return text;
	}

	get REQUIRED(): string {
		return this.getText('required');
	}
	get save(): string {
		return this.getText('save');
	}
	get close(): string {
		return this.getText('close');
	}
}
