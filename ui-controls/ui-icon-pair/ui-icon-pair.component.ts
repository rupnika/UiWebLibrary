import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

@Component({
	selector: 'ui-icon-pair',
	templateUrl: './ui-icon-pair.component.html',
	styleUrls: ['./ui-icon-pair.component.scss'],
})
export class UiIconPairComponent implements OnInit {
	// IN
	@Input() icon: IconDefinition;
	@Input() cssClass: string;
	@Input() text: string;
	@Input() textOnRightSide: boolean = true;

	// OUT
	@Output() onIconClick: EventEmitter<any> = new EventEmitter();

	@Input() title: string;
	@Input() textCssClass: string;

	constructor(public el: ElementRef, public ren: Renderer2) {}

	ngOnInit(): void {
		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}

	// Emits when icon clicked
	emitClick(event: any): void {
		this.onIconClick.emit(event);
	}
}
