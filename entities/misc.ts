import { InjectionToken } from '@angular/core';
import { HttpStatusCode, UiLanguage } from './enums';

export const UI_LANG = new InjectionToken<UiLanguage>(UiLanguage.English);
export const UI_DIGITS_DEFAULT = new InjectionToken<number>('UI_DIGITS_DEFAULT');
export const UI_DIGITS_SEPARATOR = new InjectionToken<string>('UI_DIGITS_SEPARATOR');

export class ApiResponse<T = Object> {
	constructor(
		public MessageHeader: string,
		public Message: string,
		public Status: HttpStatusCode,
		public StatusText: string,
		public Payload?: T
	) {}
}

// export class ApiResponseNew<T> {
// 	constructor(
// 		public MessageHeader: string,
// 		public Message: string,
// 		public Status: HttpStatusCode,
// 		public StatusText: string,
// 		public Payload?: T
// 	) {}
// }

export class ApiResponseException implements Error {
	constructor(public message: string, public name: string, public status: number, public stack?: string) {}
}
export class IconKeyValuePair {
	/**
	 *
	 */
	key: string;
	value: any;
	/** Used if icon not found */
	isMissing?: boolean = false;
	/** Used in FORMNET instead of letter O */
	isLogo?: boolean = false;
}
export class Constants {
	/**
	 * dd.MM.yyyy
	 */
	static dateFormat = 'dd.MM.yyyy';
	/**
	 * HH:mm:ss
	 */
	static timeFormat = 'HH:mm:ss';
	/**
	 * dd.MM.yyyy HH:mm:ss
	 */
	static dateTimeFormat = 'dd.MM.yyyy HH:mm:ss';
}

export class FileHolder {
	EditedDate: string;
	FileBase64: string;
	FileName: string;
	FileType: string;
	Size: string;
	Size_b: number;
	Size_kb: number;
}
