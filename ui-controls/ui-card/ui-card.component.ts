import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewEncapsulation,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { UiIconService } from '../../services/ui-icon.service';

@Component({
	selector: 'ui-card',
	templateUrl: './ui-card.component.html',
	styleUrls: ['./ui-card.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class UiCardComponent implements OnInit {
	@Input() public headerText: string;
	@Input() public cardClass: string;
	@Input() public titleClass: string;
	@Input() public cssClass: string;
	@Input() public cssClassContent: string;
	@Input() public flatLayout: boolean = false;
	@Input() public overflow: boolean = false;
	@Input() public overflowCardContent: boolean = false;
	@Input() public showHeader: boolean = true;

	// Custom form chekbox input
	@Input() public enableHeaderCheckbox = false;
	@Input() public headerCheckboxText = '';
	@Input() formCtrlName: string;
	@Input() myForm: FormGroup;
	@Output() onCheckboxChanged: EventEmitter<any> = new EventEmitter();

	constructor(public el: ElementRef, public ren: Renderer2, public iconSvc: UiIconService) {}

	ngOnInit(): void {}

	ngAfterViewInit() {
		this.ren.addClass(this.el.nativeElement, 'ui-card-element');

		if (this.overflow) {
			this.ren.addClass(this.el.nativeElement, 'overflow-y');
		}

		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}

	public getCardDivCssClasses() {
		if (this.cssClass) {
			return this.cssClass;
		}
		return '';
	}

	public getCardCssClasses() {
		if (this.cardClass) {
			return this.cardClass;
		}
		return '';
	}

	public getCardTitleClasses() {
		if (this.titleClass) {
			return this.titleClass;
		}
		return '';
	}

	public getCardContentClasses() {
		let classes = '';

		if (!_.isNil(this.cssClassContent)) {
			classes = `${this.cssClassContent} `;
		}
		if (this.overflow) {
			classes += ` card-content-flex`;
		}
		if (this.overflowCardContent) {
			classes += ' overflow-y-auto';
		}
		return classes;
	}
}
