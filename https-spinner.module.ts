import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { NgHttpLoaderComponent } from './http-spinner/components/ng-http-loader.component';
import { PendingRequestsInterceptorProvider } from './http-spinner/services/pending-requests-interceptor.service';
import { SPINKIT_COMPONENTS } from './http-spinner/spinkits';
import { UiIconService } from './services/ui-icon.service';

@NgModule({
	declarations: [NgHttpLoaderComponent, ...SPINKIT_COMPONENTS],
	imports: [CommonModule],
	exports: [NgHttpLoaderComponent, ...SPINKIT_COMPONENTS],
	providers: [UiIconService],
})
export class UiHttpSpinnerModule {
	public static forRoot(): ModuleWithProviders<UiHttpSpinnerModule> {
		return {
			ngModule: UiHttpSpinnerModule,
			providers: [PendingRequestsInterceptorProvider],
		};
	}
}
