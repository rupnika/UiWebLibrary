import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { UiButtonColor, UiButtonType } from '../../entities/ui-controls';
import { SpinnerHttpService } from '../../http-spinner/services/spinner-http.service';
import { SpinnerVisibilityService } from '../../http-spinner/services/spinner-visibility.service';
import { UiIconService } from '../../services/ui-icon.service';

@Component({
	selector: 'ui-button',
	templateUrl: './ui-button.component.html',
	styleUrls: ['./ui-button.component.scss'],
	host: { class: 'ui-button' },
})
export class UiButtonComponent implements OnInit {
	@Input() faIcon: IconDefinition;
	@Input() text: string;
	@Input() cssButtonClass: string;
	@Input() isIconOnRightSide: boolean = false;
	@Input() isVisible: boolean = true;
	@Input() isDisabled: boolean = false;
	@Input() disableOnApiRequest: boolean = true;
	@Input() buttonType: UiButtonType = 'flat';
	@Input() buttonColor: UiButtonColor = 'primary';
	@Output() onClick = new EventEmitter<any>();

	public faIconInternal: IconDefinition;

	constructor(
		public el: ElementRef,
		public ren: Renderer2,
		public spinnerService: SpinnerVisibilityService,
		public spinnerHttp: SpinnerHttpService,
		private iconService: UiIconService
	) {}

	ngOnInit(): void {
		this.faIconInternal = this.faIcon;
	}

	ngAfterViewInit() {}

	getButtonColor(): ThemePalette {
		if (this.buttonColor === 'primary') {
			return 'primary';
		} else if (this.buttonColor === 'secondary') {
			return 'accent';
		} else if (this.buttonColor === 'warn') {
			return 'warn';
		}
		return undefined;
	}
	getButtonInlineStyle() {
		let style = {
			display: 'flex',
			justifyContent: 'center',
		};

		if (!this.isVisible) {
			style.display = 'none';
		}
		return style;
	}
	getButtonClass(isHttpActive) {
		if (!this.isVisible) {
			return '';
		}

		let cssClass = this.cssButtonClass || '';

		if (this.buttonColor === 'link') {
			cssClass += 'ui-button-link';
		} else if (this.buttonColor === 'transparent') {
			cssClass += 'ui-button-transparent';
		}
		if (this.disableOnApiRequest) {
			if (isHttpActive || this.isDisabled) {
				this.faIconInternal = this.iconService.getIconByKey('fasBan');
				cssClass += ' button-disabled';
			} else {
				this.faIconInternal = this.faIcon;
			}
		} else {
			this.faIconInternal = this.faIcon;
		}
		return cssClass;
	}

	getButtonTextClass() {
		if (!this.isVisible) {
			return '';
		}

		if (this.buttonColor === 'link' || this.buttonType === 'stroked') {
			return 'ui-button-link';
		} else {
			return 'ui-button-text';
		}
	}
	onClickInternal() {
		if (!this.isDisabled) {
			this.onClick.emit();
		}
	}
}
