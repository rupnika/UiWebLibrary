// import * as dayjs from 'dayjs';
import dayjs from 'dayjs';

export enum DateType {
	StartOfDay,
	EndOfDay,
}
/**
 * Date generation metadata.
 */
export class DateInfo {
	/**
	 *
	 * @param date Date to use. Leave empty to use today's date.
	 * @param type Type of the new date.
	 * @param addDays Days to add.
	 */
	constructor(
		public date?: Date,
		public type?: DateType,
		public addDays?: number,
		public day?: number,
		public month?: number
	) {}
}

/**
 * Utility class.
 */
export class Utility {
	/**
	 * Convert base64 string to byte array.
	 * @param base64 String to convert.
	 */
	public static base64ToArrayBuffer(base64: string) {
		const binaryString = window.atob(base64); // Comment this if not using base64
		const bytes = new Uint8Array(binaryString.length);
		return bytes.map((byte, i) => binaryString.charCodeAt(i));
	}

	/**
	 * Converts base64 to string.
	 * @param base64 String to convert.
	 * @param escape Escape string before encoding it.
	 */
	public static base64ToString(base64: string, escape: boolean = false) {
		// if (escape) {
		// 	return decodeURIComponent(atob(base64));
		// } else {
		return atob(base64);
		// }
	}
	/**
	 * Converts string to base64 string.
	 * @param base64 String to convert.
	 * @param escape Escape string before encoding it.
	 */
	public static stringToBase64(str: string, escape: boolean = false) {
		if (escape) {
			return btoa(unescape(encodeURIComponent(str)));
		} else {
			return btoa(str);
		}
	}

	/**
	 * Converts UTF8 string to base64 string.
	 * @param str
	 * @returns
	 */
	public static stringUnicodeToBase64(str: string): string {
		const re = encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
			return String.fromCharCode(parseInt(p1, 16));
		});
		var res = btoa(re).replace('/', '-');
		return res;
	}
	/**
	 * Converts base64 string to UTF8 string.
	 * @param base64Str
	 * @returns
	 */
	public static base64ToUnicodeString(base64Str: string): string {
		const str = atob(base64Str.replace('-', '/'));
		return decodeURIComponent(
			Array.prototype.map
				.call(str, function (c) {
					return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
				})
				.join('')
		);
	}

	/**
	 * Converts arrayBuffer to base64
	 * @param arrayBuffer
	 */
	public static arrayBufferToBase64(arrayBuffer: ArrayBuffer): string {
		// var base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(arrayBuffer)));
		const base64String = btoa(
			new Uint8Array(arrayBuffer).reduce(function (data, byte) {
				return data + String.fromCharCode(byte);
			}, '')
		);
		return base64String;
	}

	public static formatSizeUnits(bytes): string {
		if (bytes >= 1073741824) {
			bytes = (bytes / 1073741824).toFixed(2) + ' GB';
		} else if (bytes >= 1048576) {
			bytes = (bytes / 1048576).toFixed(2) + ' MB';
		} else if (bytes >= 1024) {
			bytes = (bytes / 1024).toFixed(2) + ' KB';
		} else if (bytes > 1) {
			bytes = bytes + ' bytes';
		} else if (bytes == 1) {
			bytes = bytes + ' byte';
		} else {
			bytes = '0 bytes';
		}
		return bytes;
	}

	/**
	 * Create new date.
	 * @param data Create metadata.
	 */
	public static getDate(data?: DateInfo) {
		let newDate = dayjs();

		if (data?.date) {
			newDate = dayjs(data.date);
		}
		let hour = 0;
		let minute = 0;
		let seconds = 0;
		let miliseconds = 0;

		if (data?.type && data?.type === DateType.EndOfDay) {
			hour = 23;
			minute = 59;
			seconds = 59;
			miliseconds = 0;
		}

		newDate = newDate
			.set('hour', hour)
			.set('minute', minute)
			.set('second', seconds)
			.set('millisecond', miliseconds);

		if (data?.day) {
			newDate = newDate.set('date', data.day);
		}
		if (data?.month) {
			newDate = newDate.set('month', data.month - 1);
		}
		if (data?.addDays) {
			newDate = newDate.add(data.addDays, 'day');
		}
		const res = newDate.toDate();
		return res;
	}

	public static toNumber(val) {
		if (typeof val === 'string') {
			return parseFloat(val.replace(',', '.'));
		}
		return val;
	}
	public static numToString(val: number) {
		return val.toString().replace('.', ',');
	}
	public static numToFixedString(val: number, decimals: number): string {
		const fixed = val.toFixed(decimals);
		return fixed.toString().replace('.', ',');
	}
}
