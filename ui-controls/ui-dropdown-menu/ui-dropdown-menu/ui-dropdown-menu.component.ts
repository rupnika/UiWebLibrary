import { Component, Input, OnInit } from '@angular/core';
import { UiIconService } from '../../../services/ui-icon.service';

@Component({
	selector: 'ui-dropdown-menu',
	templateUrl: './ui-dropdown-menu.component.html',
	styleUrls: ['./ui-dropdown-menu.component.scss'],
})
export class UiDropdownMenuComponent implements OnInit {
	@Input() text: string;
	@Input() isVisible: boolean = true;
	constructor(public iconSvc: UiIconService) {}

	ngOnInit(): void {}

	getButtonInlineStyle() {
		let style = {
			display: 'flex',
			justifyContent: 'center',
		};

		return style;
	}
}
