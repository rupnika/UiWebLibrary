import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import * as _ from 'lodash';
import { UiMenuItem } from '../../entities/ui-controls';
import { UiIconService } from '../../services/ui-icon.service';

@Component({
	selector: 'ui-menu',
	templateUrl: './ui-menu.component.html',
	styleUrls: ['./ui-menu.component.scss'],
	encapsulation: ViewEncapsulation.None,
	host: { class: 'ui-menu-component' },
})
export class UiMenuComponent implements OnInit {
	@Input() menuItems: UiMenuItem[];
	@Input() logo: string;
	@Input() cssClass: string;
	@Input() isMobile: boolean = false;
	@Input() autoToggleItems: boolean = true;
	@Input() autoGenerateIds: boolean = true;
	@Output() onMenuClicked = new EventEmitter<UiMenuItem>();

	myReg = /\s/g;

	get normalMenuItems(): UiMenuItem[] {
		if (_.isNil(this.menuItems)) {
			return [];
		}
		const normal = this.menuItems.filter((x) => x.IsSpecial == false);
		return normal;
	}

	get specialMenuItems(): UiMenuItem[] {
		if (_.isNil(this.menuItems)) {
			return [];
		}
		const special = this.menuItems.filter((x) => x.IsSpecial == true);
		return special;
	}

	constructor(public el: ElementRef, public ren: Renderer2, public iconSvc: UiIconService, public router: Router) {}

	ngOnInit(): void {
		if (this.autoGenerateIds) {
			this.setId(this.menuItems);
		}
	}
	ngAfterViewInit() {
		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
		if (this.isMobile) {
			this.ren.addClass(this.el.nativeElement, 'mobile');
		}
	}

	onRootClicked(item: UiMenuItem) {
		if (item.RouterLink) {
			this.resetSelected(this.menuItems, item.Id);
			this.router.navigate([item.RouterLink]);
		}

		if (this.autoToggleItems) {
			item.IsExpanded = !item.IsExpanded;
		}

		item.LogTitle = item.Title;

		this.onMenuClicked.emit(item);
	}

	onChildClicked(child: UiMenuItem) {
		this.resetSelected(this.menuItems, child.Id);
		this.router.navigate([child.RouterLink]);

		let stitle = '';
		this.menuItems.forEach((x) => {
			let item = x.Childs.find((y) => y.Id == child.Id);
			if (item) {
				stitle = `${x.Title} - ${child.Title}`;
			}
		});

		child.LogTitle = stitle;
		this.onMenuClicked.emit(child);
	}

	setId(items: UiMenuItem[]) {
		if (!items) {
			return;
		}
		items.forEach((x: UiMenuItem) => {
			x.Id = Guid.create().toString();
			this.setId(x.Childs);
		});
	}
	resetSelected(items: UiMenuItem[], clickedItemId: string) {
		if (!items) {
			return;
		}

		items.forEach((x: UiMenuItem) => {
			if (x.RouterLink) {
				x.IsSelected = clickedItemId == x.Id;
			}
			this.resetSelected(x.Childs, clickedItemId);
		});
	}

	getSelectedClass(child: UiMenuItem) {
		if (child.IsSelected) {
			return 'primary-text';
		}

		return '';
	}
	getToggleStyle(menuItem: UiMenuItem) {
		let style = {
			display: 'none',
		};

		if (menuItem.IsExpanded) {
			style.display = 'flex';
		}
		return style;
	}
}
