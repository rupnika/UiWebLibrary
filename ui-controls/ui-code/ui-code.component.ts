import { Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
	selector: 'ui-code',
	templateUrl: './ui-code.component.html',
	styleUrls: ['./ui-code.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class UiCodeComponent implements OnInit {
	@Input() cssClass: string;
	@Input() codeText: string;
	@Input() codeLanguage: string;

	onHighlighted: Subject<any> = new Subject();

	constructor(public el: ElementRef, public ren: Renderer2) {}

	ngOnInit(): void {}

	ngAfterViewInit() {
		this.ren.addClass(this.el.nativeElement, 'ui-code');
		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}

	_onHighlighted(event) {
		this.onHighlighted.next(event);
	}
}
