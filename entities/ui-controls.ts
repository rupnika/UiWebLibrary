/**
 *
 */
import { ComponentType } from '@angular/cdk/portal';
import { ElementRef } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TooltipPosition } from '@angular/material/tooltip';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { EventEmitter } from 'events';
import { Guid } from 'guid-typescript';
import * as _ from 'lodash';
import { TablePageDataVm } from 'projects/Admin/src/app/shared/common/pageSize';
import { Subject } from 'rxjs/internal/Subject';
import {
	UiDialogPosition,
	UiSnackBarStatus,
	UiTableCellContentAlign,
	UiTableCellDataType,
	UiTableFilterCondition,
	UiTableIconAlign,
} from './enums';

/** Is table row selected */
export const TABLE_IS_SELECTED = 'IsSelected';
/** Is table row permanently selected */
export const TABLE_IS_SELECTED_PERMA = 'IsSelectedPerma';
/** */
export const INLINE_EDIT_ENABLED = 'INLINE_EDIT_ENABLED';
export const DROPDOWN_SHOW_ICON = 'DROPDOWN_SHOW_ICON';
export const DROPDOWN_ICON = 'DROPDOWN_ICON';
/** Table tree row ID. This property is auto-generated and is only should be used
 * only for component internal use! */
export const TABLE_ROWID = 'TABLE_ROWID';
/** Table tree child row reference to parent row ID. This property is auto-generated
 * and is only should be used only for component internal use! */
export const TABLE_ROWID_PARENT = 'TABLE_ROWID_PARENT';
export const TABLE_ROW_ORIG = 'TABLE_ROW_ORIG';
export const TABLE_ROW_ISEXPANDED = 'TABLE_ROW_ISEXPANDED';
export const TABLE_ROW_ISVISIBLE = 'TABLE_ROW_ISVISIBLE';
export const TABLE_ROW_ISDISABLED = 'TABLE_ROW_ISDISABLED';
export const TABLE_ROW_ISPARENT = 'TABLE_ROW_ISPARENT';
export const TABLE_ROW_ISCHILD = 'TABLE_ROW_ISCHILD';
export const TABLE_ROW_HASCHILDS = 'TABLE_ROW_HASCHILDS';

export class UiTableCache {
	cached: UiTableCacheEntry[];

	public getCellId(col: UiTableColumn, row: any): string {
		return `${row[TABLE_ROWID]}-${col.id}}`;
	}
	public getCachedCell(col: UiTableColumn, row: any): UiTableCacheEntry {
		const id = this.getCellId(col, row);
		return this.cached.find((x) => x.key === id);
	}
	public addCacheEntry(key: string, value: string) {
		console.log('ADD', key, value);
		this.cached.push({ key: key, value: value });
	}
}
export class UiTableCacheEntry {
	key: string;
	value: string;
}

/**
 * UiTableComponent configuration.
 * T is type of the object for table rows.
 * K is type of the object for child rows (only for tableTree)
 */
export class UiTableConiguration<T, K = void> {
	/** UiTableComponent columns definition */
	columns: UiTableColumn[];
	columnsFilters: UiTableColumn[];
	/** Instance of data source. */
	dataSource: MatTableDataSource<T>;
	/** Table header configuration */
	header: UiTableHeader;
	/** Table footer configuration */
	paginator: UiTablePaginator;
	/** */
	table?: UiTable;
	/** */
	tableTree?: UiTableTree;
	checkbox?: UiTableCheckbox;
	/** */
	checkboxSelectedRows: T[];
	alternatingColor: boolean;

	matSortStart: string = 'asc';

	// =========================
	public onDataInitialized: Subject<UiDataInitialized>;
	public onDeselectAllRows = new Subject();
	public onSelectCheckboxes = new Subject();
	public onSelectCheckboxesChilds = new Subject();

	public initEmitter = new EventEmitter();

	public onExportFilters = new Subject();

	public onImportFilters: Subject<UiTableFilters> = new Subject();
	public onSort: Subject<Sort> = new Subject();

	constructor() {
		this.header = new UiTableHeader();
		this.paginator = new UiTablePaginator();
		this.table = new UiTable();
		this.checkbox = new UiTableCheckbox();
		this.tableTree = new UiTableTree();
		this.alternatingColor = true;
		this.onDataInitialized = new Subject();
	}

	public initDataSource(data: T[], filters?: UiTableFilters, activeSort?: Sort) {
		this.dataSource = new MatTableDataSource<T>();
		let dataSource = null;

		if (this.tableTree.enabled) {
			dataSource = this._generateTableTreeDataSource(data);
		} else {
			data.forEach((x) => {
				if (!_.has(x, TABLE_ROWID)) {
					x[TABLE_ROWID] = Guid.create();
				}
				if (!_.has(x, TABLE_ROW_ISPARENT)) {
					x[TABLE_ROW_ISPARENT] = true;
				}
				if (!_.has(x, TABLE_ROW_HASCHILDS)) {
					x[TABLE_ROW_HASCHILDS] = false;
				}
				if (!_.has(x, TABLE_ROW_ISDISABLED)) {
					x[TABLE_ROW_ISDISABLED] = false;
				}
			});
			dataSource = data;
		}

		this.onDataInitialized.next({ data: dataSource, activeFilters: filters, activeSort: activeSort });
	}

	public sortTable(sort: Sort) {
		this.onSort.next(sort);
	}
	public deselectAllRows() {
		//  this.onDeselectAllRows.next();
		this.dataSource.data.forEach((x) => {
			x[TABLE_IS_SELECTED] = false;
		});
	}
	public selectChecboxes(rows: T[], isChild: boolean = false) {
		isChild ? this.onSelectCheckboxesChilds.next(rows) : this.onSelectCheckboxes.next(rows);
	}
	public getChekboxSelectedRows() {}

	public updateCellValue(row, val, fieldName, childFieldName?) {
		const rowToUpdate = this.dataSource.data.find((x) => x[TABLE_ROWID] === row[TABLE_ROWID]);

		if (fieldName) {
			rowToUpdate[fieldName] = val;
		}
		// update original child data (table tree only)
		else if (childFieldName) {
			rowToUpdate[TABLE_ROW_ORIG][childFieldName] = val;
		}

		this.initDataSource(this.dataSource.data, null, null);
	}

	public exportFilters() {
		this.onExportFilters.next();
	}

	public importFilters(filters: UiTableFilters) {
		this.onImportFilters.next(filters);
	}

	//#region ========== Helpers ==========

	private _generateTableTreeDataSource(tableData: T[]): T[] {
		let newTableData: T[] = [];
		// get visible columns for parents
		const visibleColumns = this.columns.map((x) => {
			if (x.isVisible) {
				return x.fieldName;
			}
		});

		// loop all parent table rows in provided data source
		tableData.forEach((parentRow: T) => {
			// push parent row to flattened data source
			parentRow[TABLE_ROWID] = Guid.create(); // ID for internal use
			parentRow[TABLE_ROW_ISEXPANDED] = false;
			parentRow[TABLE_ROW_ISPARENT] = true;
			parentRow[TABLE_ROW_ISDISABLED] = false;

			const childRows = parentRow[this.tableTree.childPropertyName];
			parentRow[TABLE_ROW_HASCHILDS] = !_.isNil(childRows) && childRows.length > 0;
			newTableData.push(parentRow);

			this._generateChildRows(parentRow, newTableData, visibleColumns);
			//parentRow[this.tableTree.childPropertyName] = null;
		});

		return newTableData;
	}
	private _generateChildRows(parentRow: T, newTableData: T[], visibleColumns: string[]) {
		if (_.isNil(this.tableTree.childPropertyName)) {
			throw new Error('Table configuration is incomplete (tableConfig.tableTree.childPropertyName is missing)!');
		}

		// add childs to flat tree
		const childRows = parentRow[this.tableTree.childPropertyName];
		const parentId = parentRow[TABLE_ROWID];
		parentRow[TABLE_ROW_ISVISIBLE] = true;

		// current row has child rows in data source
		if (!_.isNil(childRows) && childRows.length > 0) {
			// loop all childs of current row
			childRows.forEach((c: K) => {
				let childEl = {} as T;
				childEl[TABLE_ROWID_PARENT] = parentId; // auto generate reference to parent row ID
				childEl[TABLE_ROW_ISCHILD] = true;
				childEl[TABLE_ROW_ISPARENT] = false;
				childEl[TABLE_ROW_ISVISIBLE] = false;
				childEl[TABLE_ROW_ISDISABLED] = false;
				childEl[TABLE_ROWID] = Guid.create(); // ID for internal use

				// loop all visible columns
				visibleColumns.forEach((vc) => {
					const column = this.columns.find((x) => x.fieldName === vc);
					const childColumnFieldName = column?.childColumn?.fieldName;

					// child 'fieldName' is defined
					if (!_.isNil(childColumnFieldName)) {
						childEl[vc] = c[childColumnFieldName];
					}
					// child 'fieldName' is not define. Use same 'fieldName' as parent row
					else {
					}
				});

				childEl[TABLE_ROW_ORIG] = c; // assign original data to child row

				// (+) This will disable the entire row if one of the columns is disabled (row manipulation is not implemented yet)
				this.columns.forEach((col) => {
					if (!_.isNil(col.childColumn.cellDisabled)) {
						if (col.childColumn.cellDisabled(c)) {
							childEl[TABLE_ROW_ISDISABLED] = true;
						}
					}
				});
				newTableData.push(childEl);
			});
		}

		// (+) If all the child rows are disabled, disable the parent row aswell
		let setDisabledParent = true;
		newTableData.forEach((row) => {
			if (!row[TABLE_ROW_ISDISABLED] && row[TABLE_ROWID_PARENT] == parentRow[TABLE_ROWID]) {
				setDisabledParent = false;
			}
		});
		parentRow[TABLE_ROW_ISDISABLED] = setDisabledParent;
	}

	//#endregion
}

export class UiTable {
	public onToggleVisibility: Subject<boolean> = new Subject();

	toggleVisibility(isVisible: boolean) {
		this.onToggleVisibility.next(isVisible);
	}
	/** Width relative to parent in px or %. */
	width: string;
	// /** Change selected row background color. */
	// selectClickedRow?: boolean;
	/** Highlight row on hower. Default=TRUE.*/
	higlightRowOnHover?: boolean = true;
	/** Keep selected row highlighted. Default=TRUE.*/
	highlightSelectedRow?: boolean = true;
	/** When hovering table row show cursor as pointer. Else use default pointer. Default=TRUE.*/
	highlightRowCursorAsPointer?: boolean = true;
	/** Enables column filtering. Filters on individual columns can also be enabled/disabled in UiTableColumn. Default=TRUE.*/
	enableColumnFiltering?: boolean = true;
	/** Default=FALSE. */
	enableRowCheckbox?: boolean;
	/* Enable export table button. Default=FALSE. */
	isExportVisible?: boolean;
	/* Define max height of table. */
	maxHeightPx?: number;
	/* Identifier for table, for filter persist config. */
	ident?: string;
	/* Every table requires manual input for this thought, default false */
	enableFilterExport?: boolean = false;
	/** Currently active filters on table. */
	activeFilters?: UiTableFilters;
	/* Currently active page size */
	activePageSize?: TablePageDataVm;
}
export class UiTableCheckbox {
	/** Enable selection of multiple checkboxes. Default=TRUE */
	multiselect: boolean = true;

	public onClearCheckboxes = new Subject();
	constructor() {
		this.multiselect = true;
	}

	/** Clear all checkboxes in current table. */
	clearSelection() {
		this.onClearCheckboxes.next();
	}
}
export class UiTableTree {
	enabled?: boolean;
	childPropertyName?: string;

	public onExpandAllParents: Subject<any>;
	public onCollapseAllParents: Subject<any>;
	public onInitTableFinished: Subject<any>;
	/**
	 *
	 */
	constructor() {
		this.onInitTableFinished = new Subject();
		this.onExpandAllParents = new Subject();
		this.onCollapseAllParents = new Subject();
	}

	public expandAllParents() {
		this.onExpandAllParents.next();
	}
	public callapseAllParents() {
		this.onCollapseAllParents.next();
	}
}
export class UiTableColumn {
	cachedText?: string;
	/** Only for internal use. DON'T set this value manually! */
	id?: string;
	/** Only for internal use. DON'T set this value manually! */
	showInline?: boolean;
	/** Column header text. */
	headerText?: string;
	/** Show cell text. Default is TRUE. */
	showText?: boolean;
	/** Show text before icon if both are provided. Default is FALSE. */
	textFirst?: boolean;
	/** Name of the property to bind to this column. */
	fieldName?: string;
	/** Internal property and should not be used manually !!! */
	fieldNameFilter?: string;
	/** Explicit column width in pixels (ex. '200px') */
	width?: string;
	/** If TRUE, column will be resized to minimal size based on header content. Default is FALSE. */
	minimumSize?: boolean;
	/** Is column visible. Default is TRUE. */
	isVisible?: boolean;
	inlineEditStringLimit?: number;
	/** Cell can be edited inline. Apply only to UiTableCellDataType.Text. Default is FALSE. */
	inlineEditEnabled?;
	/** Cell will be edited inline on single click. */
	singleClickInlineEdit?;
	/** Incase of inline editing shows accept and reject new value. */
	inlineEditShowSideButtons?: boolean;
	/** Inline editing is finished. */
	onInlineEditFinished?;
	/** Is column visible on mobile viewport size. Default is TRUE. */
	showOnMobile?: boolean;
	/** Type of the data that will be shown in this column. Default is TEXT. */
	cellDataType?: UiTableCellDataType;
	cellNumOfDigits?: number;
	/** Type of the data that will be shown when filtering by this column. If not specified, it will be the same as cellDataType. */
	filterDataType?: UiTableCellDataType;
	/** OBSOLETE! Will be removed eventually. */
	cellContentAlign?: UiTableCellContentAlign;
	/** List of icons to be show in this column. */
	cellIcons?: UiTableCellIconConfiguration[];
	/** Enable filtering on this column. Default is TRUE. */
	filterEnabled?: boolean;
	/** Enable sorting of this column. Default is TRUE. */
	sortingEnabled?: boolean;
	/** */
	tooltipDelay?: number = 1000;
	/** Click on table row will be registered. */
	registerClick?: boolean;
	/** Function to be executed that will return custom text to be shown in this column. */
	cellText?;
	cellDisabled?;
	cellColor?;
	/** Cell icons alignment. */
	cellIconAlign?: UiTableIconAlign;
	/** Expand cell icon to max. size or not? */
	cellIconMinSize?: boolean;
	/** Table cell dropdown configuration. Only applied with cellDataType = UiTableCellDataType.Dropdown. */
	dropdown?: UiTableDropdownConfig<any>;
	highlightOnHover?;
	/** */
	childColumn?: UiTableColumn;
	/* Set link to route to when cell text is clicked */
	routerLink?;
	/* Defines if date column is shown in locale time or not(ex. UTC), default values is TRUE! */
	showDateInLocalTime?: boolean;
	/* CheckBox for selecting or deselcting all switch buttons */
	includeExcludeEnabled?: boolean;
	/** Executes the provided function that returns cell tooltip text. */
	getTooltipText?: any;
	/** Show cell tooltip. Default is FALSE. */
	cellTooltipEnabled?: boolean;
	/** Cell tooltip position. */
	cellTooltipPosition?: TooltipPosition;
}
export class UiTableHeader {
	/** Show table header. Default=TRUE. */
	showHeader?: boolean = true;
	/** Always show table header. Default=TRUE. */
	stickyHeader?: boolean = true;
	/** Overrides the default mat-header-row element styling. */
	cssClassHeaderRow?: string;
	/** Override the default mat-header-cell element styling. */
	cssClassHeaderCell?: string;
}
export class UiTablePaginator {
	/** Show or hide paginator. Default is TRUE. */
	showPaginator?: boolean = true;
	/** Total table row count per page */
	pageSize?: number;
	/** Page size options shown in paginator dropdown menu. */
	pageSizeOptions?: number[];
	/** Show or hide paginator on the bottom of the table. */
	paginatorCssClass?: string;
	/** Show or hide page options. */
	paginatorShowPages?: boolean = true;
}
export class UiTableCellIconConfiguration {
	/** Change cursor on hover to 'hand' */
	isRouterLink?: boolean = false;
	/** Is icon visible method. */
	isVisible?;
	/** Executes the provided function that returns icon. */
	getIcon;
	/** Executes the provided function that returns icon CSS class. */
	getIconClass;
	/** Executes the provided function that returns icon inline CSS style. */
	getIconStyle?;
	/** Executes the provided function that returns icon tooltip text. */
	getTooltipText;
	/** Executes the provided function that is executed when icon is clicked. */
	onClick;
	/** Special icon is added so we can display picked icon from icon picker. */
	specialIcon?;
	/* */
	isLinkIcon?: boolean = false;
	/* Icon router link */
	iconHref?;
	/* Icon identifier if nedeed */
	iconId?: string;
}
export class UiDropdownSelectEmit<T> {
	constructor(public value: T, public emitChange: boolean = true) {}
}
export class UiDataInitialized {
	activeFilters: UiTableFilters;
	activeSort?: Sort;
	data?: any;
}
export class UiDropdownConfig<T> {
	/**Dropdown autcomplete readOnly */
	public readOnly?: boolean = false;
	/** Label. */
	public label?: string;
	/** Can custom input be added to dropdown values? Default = false.  */
	public allowNewEntry?: boolean;
	/** Dropdown data. */
	public dataInternal: UiDropdownEntry[];
	public data: T[];

	/** Autocomplete. Default = TRUE */
	public autocomplete: boolean;
	public isDisabled: boolean;
	public onDataInitialized: Subject<any>;
	public onSelectByIndex = new Subject<UiDropdownSelectEmit<number>>();
	public onSelectById = new Subject<UiDropdownSelectEmit<any>>();
	public onSelectByValue = new Subject<UiDropdownSelectEmit<string>>();
	public onClearDropdown = new Subject<any>();

	private idFieldName: string;
	get idField() {
		return this.idFieldName;
	}
	private valueFieldName: string;
	get valueField() {
		return this.valueFieldName;
	}

	set idField(id: string) {
		this.idFieldName = id;
	}

	set valueField(value: string) {
		this.valueFieldName = value;
	}

	constructor(label?: string) {
		this.onDataInitialized = new Subject();

		this.label = label || '';
		this.dataInternal = [];
		this.data = [];
		this.autocomplete = true;
		this.allowNewEntry = false;
		this.isDisabled = false;
	}

	public initDropdown(id?: string, display?: string, data: T[] = null) {
		this.idFieldName = id;
		this.valueFieldName = display;

		if (!_.isNil(data)) {
			this.data = data;
		}
		if (!_.isNil(this.data)) {
			this.dataInternal = this.data.map((x: T) => {
				const entry = {
					id: _.isNil(id) ? `id-${Guid.create()}` : x[id],
					value: _.isNil(display) || display === '' ? x : x[display],
				};
				return entry;
			});
		}
		this.onDataInitialized.next();
	}

	public clearSelection() {
		this.onClearDropdown.next();
	}
	public getDataInternalById(id): UiDropdownEntry {
		const res = this.dataInternal.find((x) => {
			if (x.id === id) {
				return x;
			}
		});
		return res;
	}
	public getDataById(id): T {
		const res = this.data.find((x) => {
			if (x[this.idFieldName] === id) {
				return x;
			}
		});
		return res;
	}
	public getDataInternalByValue(value): UiDropdownEntry {
		const res = this.dataInternal.find((x) => {
			if (x.value === value) {
				return x;
			}
		});
		return res;
	}
	public getDataByValue(value): T {
		const res = this.data.find((x) => {
			if (x[this.valueFieldName] === value) {
				return x;
			}
		});
		return res;
	}

	public selectByIndex(index: number, emitChange: boolean = true) {
		const data = new UiDropdownSelectEmit(index, emitChange);
		this.onSelectByIndex.next(data);
	}
	public selectById(id: any, emitChange: boolean = true) {
		const data = new UiDropdownSelectEmit(id, emitChange);
		this.onSelectById.next(data);
	}
	public selectByValue(value: string, emitChange: boolean = true) {
		const data = new UiDropdownSelectEmit(value, emitChange);
		this.onSelectByValue.next(data);
	}
}

export class UiTableDropdownConfig<T> {
	public data: T[];
	public idField: string;
	public valueField: string;
}
export class UiDropdownEntry {
	id: any;
	value: string;
}
export class UiTableCellDropdownChanged {
	value: any;
	row: any;
}
export class UiTableExcludeOrIncludeChanged {
	value: boolean;
	column: UiTableColumn;
}
export class UiSnackBarData {
	constructor(public message: string, public status: UiSnackBarStatus) {}
}
export class UiConfirmDialogData {
	constructor(
		public text?: string,
		public boldText?: string,
		public showOk?: boolean,
		public showCancel?: boolean,
		public okText?: string,
		public cancelText?: string,
		/** Keyboard shortcut for confirm button. */
		public keybindConfirm?: string,
		/** Keyboard shortcut for cancel button. */
		public keybindCancel?: string,
		public icon?: IconDefinition
	) {}
}
export class UiNotifyDialogData {
	constructor(public text?: string, public closeText?: string) {}
}

export class UiDialogConfiguration {
	/** Component to show in dialog */
	component: ComponentType<any>;
	/**  */
	parentRef?: ElementRef;
	/** Show backdrop when dialog is opened. */
	hasBackDrop?: boolean = true;
	/** Can the dialog be closed with click on backdrop. */
	canCloseOnBackdrop?: boolean = false;
	position?: UiDialogPosition = UiDialogPosition.Default;
	paddingRight?: string;
	paddingBottom?: string;
	paddingTop?: string;
	paddingLeft?: string;
	height?: string;
	width?: string;
	heightDelta?: number = 0;
	widthDelta?: number = 0;
	dialogClass?: string;
}
export class UiConfirmDialogConfiguration {
	hasBackDrop?: boolean;
	/** Can the dialog be closed with click on backdrop. */
	canCloseOnBackdrop?: boolean = false;
	position?: UiDialogPosition;
	parentRef?: ElementRef;
	text: string;
	boldText?: string;
	data?: UiConfirmDialogData;
	/** Keyboard shortcut for confirm button. */
	public keybindConfirm?: string;
	/** Keyboard shortcut for cancel button. */
	public keybindCancel?: string;
	height?: string;
	width?: string;
	percentableSize?: boolean;
	paddingRight?: string;
	paddingBottom?: string;
	paddingTop?: string;
	paddingLeft?: string;
	icon?: IconDefinition;
}
export class UiNotifyDialogConfiguration {
	hasBackDrop?: boolean;
	/** Can the dialog be closed with click on backdrop. */
	canCloseOnBackdrop?: boolean = false;
	position?: UiDialogPosition;
	parentRef?: ElementRef;
	text: string;
	data?: UiNotifyDialogData;
	height?: string;
	width?: string;
}

export class UiTableFilterArgs {
	constructor() {
		this.filterDataType = UiTableCellDataType.Text;
	}
	field: string;
	fieldFilter: string;

	filterDataType: UiTableCellDataType;
	filterCondition?: UiTableFilterCondition;
	filterText: string;
	filterDropdown: string[];
	filterDropdownSelected: string;
	filterDateFrom: Date;
	filterDateTo: Date;
	filterBool: boolean;
}
export class UiFormError {
	/**
	 *
	 */
	constructor(public rule: string, public message: string) {}
}

export class UiMenuItem {
	Id: string;
	Title: string;
	RouterLink: string;
	ModuleKey: string;
	IconKey: string;
	FaIcon: IconDefinition;
	IsSelected: boolean;
	IsExpanded: boolean;
	IsSpecial: boolean = false;
	Childs: UiMenuItem[];
	LogTitle: string;
}

export type UiButtonType = 'basic' | 'raised' | 'stroked' | 'flat';
export type UiButtonColor = 'primary' | 'secondary' | 'warn' | 'link' | 'transparent';

export type UiFormInputType =
	| 'input'
	| 'password'
	| 'email'
	| 'textarea'
	| 'number'
	| 'datetime'
	| 'dropdown'
	| 'dropdown-2'
	| 'checkbox'
	| 'file'
	| 'fileButton'
	| 'date';

/** Set visibility for action buttons on dual list */
export class UiDualListConfig {
	constructor(
		public canMoveAllLeft: boolean = true,
		public canMoveAllRight: boolean = true,
		public canMoveLeft: boolean = true,
		public canMoveRight: boolean = true
	) {}
}

/** UiDualList events. */
export enum UiDualListEventType {
	MOVED_LEFT,
	MOVED_RIGHT,
	MOVED_ALL_LEFT,
	MOVED_ALL_RIGHT,
}

/** Use for emiting events on dual list */
export class UiDualListEvent {
	constructor(public eventType: UiDualListEventType, public info: string, public data: any) {}
}

export class UiTableFilters {
	tableId: string;
	activeRoute: string;
	toRoute: string;
	filters: string;
}

export class UiTableSort {
	tableId: string;
	sort: Sort;
}

export class NewTabClickedVm {
	icon: UiTableCellIconConfiguration;
	row: any;
}

export class UiTableCellClicked {
	/**
	 *
	 */
	constructor(public routerLink?: string, public row?: any, public column?: UiTableColumn) {}
}
