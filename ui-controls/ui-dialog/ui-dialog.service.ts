import { HostListener, Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import * as _ from 'lodash';
import { UiDialogPosition } from '../../entities/enums';
import { UiConfirmDialogConfiguration, UiConfirmDialogData, UiDialogConfiguration } from '../../entities/ui-controls';
import { UiNotifyDialogConfiguration, UiNotifyDialogData } from './../../entities/ui-controls';
import { UiTextService } from './../../services/ui-text.service';
import { UiConfirmDialogComponent } from './ui-confirm-dialog/ui-confirm-dialog.component';
import { UiNotifyDialogComponent } from './ui-notify-dialog/ui-notify-dialog.component';

@Injectable({
	providedIn: 'root',
})
export class UiDialogService {
	constructor(public dialog: MatDialog, public textSvc: UiTextService) {}
	height: string;
	width: string;

	@HostListener('window:scroll', [])
	onScroll(): boolean {
		if (scrollY > 150) return true;
		else return false;
	}

	//Pozicija starša (html element)
	private ParentPosition = {
		top: 0,
		bottom: 0,
		right: 0,
		left: 0,
		width: 0,
		height: 0,
	};

	/**
	 * Funkcija za odpiranje dialoga s custom komponento.
	 * @param component Komponenta ki se bo prikazala v dialogu.
	 * @param config Konfiguracija dialoga.
	 * @param nData Podatki poslani v komponento.
	 */
	public openDialog(config: UiDialogConfiguration, nData?: any): MatDialogRef<any> {
		var matCfg = {
			data: _.isNil(nData) ? null : nData,
			hasBackdrop: _.isNil(config.hasBackDrop) ? true : config.hasBackDrop,
			disableClose: !config.canCloseOnBackdrop,
			panelClass: config.dialogClass,
			height: '',
			width: '',
			position: { top: '', right: '', left: '', bottom: '' },
		};
		// default position is TOP if not specified
		config.position = _.isNil(config.position) ? UiDialogPosition.Top : config.position;

		this.ParentPosition = {
			top: _.isNil(config.parentRef) ? 0 : config.parentRef.nativeElement.getBoundingClientRect().top,
			bottom: _.isNil(config.parentRef)
				? window.innerWidth
				: config.parentRef.nativeElement.getBoundingClientRect().bottom,
			right: _.isNil(config.parentRef)
				? window.innerHeight
				: config.parentRef.nativeElement.getBoundingClientRect().right,
			left: _.isNil(config.parentRef) ? 0 : config.parentRef.nativeElement.getBoundingClientRect().left,
			height: _.isNil(config.parentRef) ? window.innerHeight : config.parentRef.nativeElement.offsetHeight,
			width: _.isNil(config.parentRef) ? window.innerWidth : config.parentRef.nativeElement.offsetWidth,
		};

		matCfg = this.prepConfig(config, matCfg);

		console.log('UiDialog config', config, matCfg);
		return this.dialog.open(config.component, matCfg);
	}

	/**
	 * Funkcija za odpiranje Confirm dialoga (potrditev izbrisa)
	 * @param config Konfiguracija dialoga
	 */
	public openConfirmDialog(config: UiConfirmDialogConfiguration) {
		let nData = new UiConfirmDialogData();
		nData.text = config.text;
		nData.boldText = config.boldText;
		nData.showOk = _.isNil(config.data?.showOk) ? true : config.data?.showOk;
		nData.showCancel = _.isNil(config.data?.showCancel) ? true : config.data?.showCancel;
		nData.okText = _.isNil(config.data?.okText) ? this.textSvc.getText('yes') : config.data.okText;
		nData.cancelText = _.isNil(config.data?.cancelText) ? this.textSvc.getText('no') : config.data.cancelText;
		nData.keybindConfirm = config.keybindConfirm;
		nData.keybindCancel = config.keybindCancel;
		nData.icon = config.icon;
		var matCfg = {
			data: nData,
			hasBackdrop: _.isNil(config.hasBackDrop) ? true : config.hasBackDrop,
			disableClose: !config.canCloseOnBackdrop,
			panelClass: '',
			height: '',
			width: '',
			position: { top: '', right: '', left: '', bottom: '' },
			paddingRight: '',
			paddingBottom: '',
			paddingTop: '',
			paddingLeft: '',
		};
		if (config.parentRef)
			this.ParentPosition = {
				top: config.parentRef.nativeElement.getBoundingClientRect().top,
				bottom: config.parentRef.nativeElement.getBoundingClientRect().bottom,
				right: config.parentRef.nativeElement.getBoundingClientRect().right,
				left: config.parentRef.nativeElement.getBoundingClientRect().left,
				height: config.parentRef.nativeElement.offsetHeight,
				width: config.parentRef.nativeElement.offsetWidth,
			};

		matCfg = this.prepConfig(config, matCfg);
		return this.dialog.open(UiConfirmDialogComponent, matCfg);
	}

	/**
	 * Funkcija za odpiranje norify dialoga
	 * @param config Konfiguracija dialoga
	 */
	public openNotifyDialog(config: UiNotifyDialogConfiguration) {
		let nData = new UiNotifyDialogData();
		nData.text = config.text;
		nData.closeText = _.isNil(config.data?.closeText) ? this.textSvc.getText('close') : config.data.closeText;

		var matCfg = {
			data: nData,
			hasBackdrop: _.isNil(config.hasBackDrop) ? true : config.hasBackDrop,
			disableClose: !config.canCloseOnBackdrop,
			panelClass: '',
			height: '',
			width: '',
			position: { top: '', right: '', left: '', bottom: '' },
			paddingRight: '',
			paddingBottom: '',
			paddingTop: '',
			paddingLeft: '',
		};
		if (config.parentRef)
			this.ParentPosition = {
				top: config.parentRef.nativeElement.getBoundingClientRect().top,
				bottom: config.parentRef.nativeElement.getBoundingClientRect().bottom,
				right: config.parentRef.nativeElement.getBoundingClientRect().right,
				left: config.parentRef.nativeElement.getBoundingClientRect().left,
				height: config.parentRef.nativeElement.offsetHeight,
				width: config.parentRef.nativeElement.offsetWidth,
			};

		return this.dialog.open(UiNotifyDialogComponent, matCfg);
	}

	public closeAll() {
		this.dialog.closeAll();
	}

	private prepConfig(config: UiDialogConfiguration | UiConfirmDialogConfiguration, matCfg: any): any {
		let isParentDefined: boolean = !_.isNil(config.parentRef);

		if (!this.onScroll()) {
			let paddingTop = config.paddingTop || 0;
			let paddingBottom = config.paddingBottom || 0;
			let paddingRight = config.paddingRight || 0;
			let paddingLeft = config.paddingLeft || 0;

			switch (config.position) {
				//DEFAULT POSITION
				case UiDialogPosition.Default: {
					// matCfg = this.prepConfig(config, matCfg);
					break;
				}
				//TOP POSITION
				case UiDialogPosition.Top: {
					matCfg.position.top = Number(this.ParentPosition.top) + Number(paddingTop) + 'px';
					break;
				}
				//TOP RIGHT POSITION
				case UiDialogPosition.Top_Right: {
					matCfg.position.top = Number(this.ParentPosition.top) + Number(paddingTop) + 'px';
					matCfg.position.right =
						window.innerWidth - Number(this.ParentPosition.right) + Number(paddingRight) + 'px';
					break;
				}
				//TOP LEFT POSITION
				case UiDialogPosition.Top_Left: {
					matCfg.position.top = Number(this.ParentPosition.top) + Number(paddingTop) + 'px';
					matCfg.position.left = Number(this.ParentPosition.left) + Number(paddingLeft) + 'px';
					break;
				}
				//BOTTOM POSITION
				case UiDialogPosition.Bottom: {
					matCfg.position.bottom =
						window.innerHeight - Number(this.ParentPosition.bottom) + Number(paddingBottom) + 'px';
					break;
				}
				//BOTTOM LEFT POSITION
				case UiDialogPosition.Bottom_Left: {
					matCfg.position.bottom =
						window.innerHeight - Number(this.ParentPosition.bottom) + Number(paddingBottom) + 'px';
					matCfg.position.left = Number(this.ParentPosition.left) + Number(paddingLeft) + 'px';
					break;
				}
				//BOTTOM RIGHT POSITION
				case UiDialogPosition.Bottom_Right: {
					matCfg.position.bottom =
						window.innerHeight - Number(this.ParentPosition.bottom) + Number(paddingBottom) + 'px';

					matCfg.position.right =
						window.innerWidth - Number(this.ParentPosition.right) + Number(paddingRight) + 'px';
					break;
				}

				// UNDER BORDER (USED ON ŠIFRANTI)
				case UiDialogPosition.Under_Border: {
					matCfg.position.top = Number(this.ParentPosition.top) + Number(paddingTop) - 49 + 'px';
					matCfg.height = window.innerHeight - Number(this.ParentPosition.top);
					break;
				}

				default: {
					break;
				}
			}
		}

		if (!_.isNil(config.height)) {
			let newheight: number;
			if (config.height.indexOf('%') !== -1) {
				let height = config.height.replace('%', '');

				if (isParentDefined) {
					newheight = Number(window.innerHeight - (Number(this.ParentPosition.top) * Number(height)) / 100);
				} else {
					newheight = Number(window.innerHeight * (Number(height) / 100));
				}
			} else {
				// '200' '200px'
				let height = config.height.replace('px', '');
				newheight = Number(height);
			}
			matCfg.height = `${newheight}px`;
		}
		if (!_.isNil(config.width)) {
			let newWidth: number;
			if (config.width.indexOf('%') !== -1) {
				let width = config.width.replace('%', '');
				if (isParentDefined) {
					newWidth = Number(window.innerWidth - (Number(this.ParentPosition.left) * Number(width)) / 100);
				} else {
					newWidth = Number(this.ParentPosition.width * (Number(width) / 100));
				}
			} else {
				// '200' '200px'
				let width = config.width.replace('px', '');
				newWidth = Number(width);
			}
			matCfg.width = `${newWidth}px`;
		}
		return matCfg;
	}
}
