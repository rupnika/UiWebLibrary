/**
 * dsf
 */
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
import { Constants } from '../entities/misc';
@Pipe({
	name: 'LocalDateTimePipe',
})
export class UiLocalDateTimePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		if (!_.isNil(value)) {
			var utcDate = new Date(value);
			var newDate = new Date(utcDate.getTime() - utcDate.getTimezoneOffset() * 60 * 1000);
			return super.transform(newDate, Constants.dateTimeFormat, 'locale');
		}
	}
}
@Pipe({
	name: 'LocalDatePipe',
})
export class UiLocalDatePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		if (!_.isNil(value)) {
			var utcDate = new Date(value);
			var newDate = new Date(utcDate.getTime() - utcDate.getTimezoneOffset() * 60 * 1000);
			return super.transform(newDate, Constants.dateFormat);
		}
	}
}
@Pipe({
	name: 'LocalTimePipe',
})
export class UiLocalTimePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		if (!_.isNil(value)) {
			var utcDate = new Date(value);
			var newDate = new Date(utcDate.getTime() - utcDate.getTimezoneOffset() * 60 * 1000);
			return super.transform(newDate, Constants.timeFormat);
		}
	}
}
