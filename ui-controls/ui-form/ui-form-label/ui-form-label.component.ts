import { Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'ui-form-label',
	templateUrl: './ui-form-label.component.html',
	styleUrls: ['./ui-form-label.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class UiFormLabelComponent implements OnInit {
	@Input() cssClass: string;
	@Input() labelText: string;
	@Input() labelTextCss: string;
	@Input() text: string;
	@Input() textCss: string;

	constructor(public el: ElementRef, public ren: Renderer2) {}

	ngOnInit(): void {}

	ngAfterViewInit() {
		this.ren.addClass(this.el.nativeElement, 'ui-form-label-element');
		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}
}
