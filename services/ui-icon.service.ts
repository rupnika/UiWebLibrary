import { Injectable } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import {
	faAngleDoubleLeft as falAngleDoubleLeft,
	faAngleDoubleRight as falAngleDoubleRight,
	faAngleDown as falAngleDown,
	faAngleLeft as falAngleLeft,
	faAngleRight as falAngleRight,
	faBan as falBan,
	faBan as fasBan,
	faCaretDown as fasCaretDown,
	faCheck as falCheck,
	faChevronDown as falChevronDown,
	faChevronDown as fasChevronDown,
	faChevronUp as falChevronUp,
	faFileCsv as falFileCsv,
	//faChevronDoubleRight as falChevronDoubleRight,
	faFilter as fasFilter,
	faFont as falCase,
	faPaperclip as falPaperclip,
	faPencilAlt as falPencilAlt,
	faQuestion as falQuestion,
	faSave as falSave,
	faTimes as falTimes,
	faTimes as fasTimes,
} from '@fortawesome/free-solid-svg-icons';
import { IconKeyValuePair } from '../entities/misc';

@Injectable({
	providedIn: 'root',
})
export class UiIconService {
	icons: IconKeyValuePair[];
	constructor() {
		let c = new IconKeyValuePair();
		this.icons = [];
		this.icons.push({ key: 'falBan', value: falBan });
		this.icons.push({ key: 'fasBan', value: fasBan });
		//this.icons.push({ key: 'falFontCase', value: falFontCase });
		this.icons.push({ key: 'falTimes', value: falTimes });
		this.icons.push({ key: 'falCheck', value: falCheck });
		this.icons.push({ key: 'falFileCsv', value: falFileCsv });
		this.icons.push({ key: 'fasChevronDown', value: fasChevronDown });
		this.icons.push({ key: 'fasFilter', value: fasFilter }); // remove
		this.icons.push({ key: 'falPaperclip', value: falPaperclip }); // remove
		this.icons.push({ key: 'falAngleRight', value: falAngleRight }); // remove
		this.icons.push({ key: 'falChevronUp', value: falChevronUp }); // remove
		this.icons.push({ key: 'falChevronDown', value: falChevronDown }); // remove
		this.icons.push({ key: 'falCase', value: falCase }); // remove
		this.icons.push({ key: 'falAngleRight', value: falAngleRight }); // remove
		this.icons.push({ key: 'falAngleDown', value: falAngleDown }); // remove
		this.icons.push({ key: 'falAngleDoubleRight', value: falAngleDoubleRight }); // remove
		this.icons.push({ key: 'falAngleLeft', value: falAngleLeft }); // remove
		this.icons.push({ key: 'falAngleDoubleLeft', value: falAngleDoubleLeft }); // remove
		this.icons.push({ key: 'fasCaretDown', value: fasCaretDown });
		this.icons.push({ key: 'falSave', value: falSave });
		this.icons.push({ key: 'falPencilAlt', value: falPencilAlt });
		this.icons.push({ key: 'fasTimes', value: fasTimes });
		//this.icons.push({ key: 'falChevronDoubleRight', value: falChevronDoubleRight });
		this.icons.push({
			key: 'falQuestion',
			value: falQuestion,
			isMissing: true,
		});
	}

	public getIconByKey(key: string): IconDefinition {
		const icon = this.icons.find((i: IconKeyValuePair) => {
			if (i.key === key) {
				return i;
			}
		});

		if (!icon) {
			return this.getIconMissing();
		}
		return icon.value;
	}
	public getIconMissing(): IconDefinition {
		const icon = this.icons.find((i: IconKeyValuePair) => {
			if (i.isMissing === true) {
				return i;
			}
		});
		return icon.value;
	}

	get falAngleRight() {
		return this.getIconByKey('falAngleRight');
	}

	get falAngleDown() {
		return this.getIconByKey('falAngleDown');
	}
	get falChevronUp() {
		return this.getIconByKey('falChevronUp');
	}
	get falChevronDown() {
		return this.getIconByKey('falChevronDown');
	}
}
