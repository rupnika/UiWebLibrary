import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UiNotifyDialogData } from '../../../entities/ui-controls';
import { UiIconService } from '../../../services/ui-icon.service';

@Component({
	selector: 'app-confirm-dialog',
	templateUrl: './ui-notify-dialog.component.html',
	styleUrls: ['./ui-notify-dialog.component.scss'],
})
export class UiNotifyDialogComponent implements OnInit {
	constructor(@Inject(MAT_DIALOG_DATA) public data: UiNotifyDialogData, public iconSvc: UiIconService) {
		console.log('NOTIFY DATA', this.data);
	}

	ngOnInit(): void {}

	/**
	 * Emitter za pregled nad potrditvijo
	 */
	onCloseClicked = new EventEmitter();

	/**
	 *  Če potrdi izbris, vrne emmit
	 */
	closeClicked() {
		this.onCloseClicked.emit();
	}
}
