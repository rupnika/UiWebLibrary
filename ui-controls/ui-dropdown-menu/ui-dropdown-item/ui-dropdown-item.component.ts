import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

@Component({
	selector: 'ui-dropdown-item',
	templateUrl: './ui-dropdown-item.component.html',
	styleUrls: ['./ui-dropdown-item.component.scss'],
})
export class UiDropdownItemComponent implements OnInit {
	@Input() text: string;
	@Input() faIcon: IconDefinition;
	@Input() isVisible: boolean = true;
	@Output() onClick = new EventEmitter<any>();

	public faIconInternal: IconDefinition;

	constructor() {}

	ngOnInit(): void {
		this.faIconInternal = this.faIcon;
	}

	_onClick() {
		this.onClick.emit();
	}
}
