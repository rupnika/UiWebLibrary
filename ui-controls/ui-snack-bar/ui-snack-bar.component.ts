/**
 *
 */
import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import {
	faCheckCircle,
	faExclamationCircle,
	faInfoCircle,
	faQuestionCircle,
	faTimesCircle,
	faWindowClose,
} from '@fortawesome/free-solid-svg-icons';
import { UiSnackBarStatus } from '../../entities/enums';
import { UiSnackBarData } from './../../entities/ui-controls';

@Component({
	selector: 'lib-ui-snack-bar',
	templateUrl: './ui-snack-bar.component.html',
	styleUrls: ['./ui-snack-bar.component.scss'],
})
export class UiSnackBarComponent implements OnInit {
	faCheckCircle = faCheckCircle; // Ok
	faInfoCircle = faInfoCircle; // Info
	faExclamationCircle = faExclamationCircle; // warning
	faTimesCircle = faTimesCircle; // Error
	faQuestionCircle = faQuestionCircle; // Question
	faWindowClose = faWindowClose; // Close

	constructor(
		public snackBarRef: MatSnackBarRef<UiSnackBarComponent>,
		@Inject(MAT_SNACK_BAR_DATA) public data: UiSnackBarData
	) {}

	getIcon() {
		if (this.data.status === UiSnackBarStatus.Ok) {
			return faCheckCircle;
		} else if (this.data.status === UiSnackBarStatus.Info) {
			return faInfoCircle;
		} else if (this.data.status === UiSnackBarStatus.Warning) {
			return faExclamationCircle;
		} else if (this.data.status === UiSnackBarStatus.Error) {
			return faTimesCircle;
		} else if (this.data.status === UiSnackBarStatus.Question) {
			return faQuestionCircle;
		}
	}
	getIconClass() {
		if (this.data.status === UiSnackBarStatus.Ok) {
			return 'ok';
		} else if (this.data.status === UiSnackBarStatus.Info) {
			return 'info';
		} else if (this.data.status === UiSnackBarStatus.Warning) {
			return 'warning';
		} else if (this.data.status === UiSnackBarStatus.Error) {
			return 'error';
		} else if (this.data.status === UiSnackBarStatus.Question) {
			return 'question';
		}
	}
	ngOnInit(): void {}
}
