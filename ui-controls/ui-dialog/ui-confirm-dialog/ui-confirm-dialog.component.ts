import { Component, EventEmitter, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UiConfirmDialogData } from '../../../entities/ui-controls';
import { UiIconService } from '../../../services/ui-icon.service';

@Component({
	selector: 'app-confirm-dialog',
	templateUrl: './ui-confirm-dialog.component.html',
	styleUrls: ['./ui-confirm-dialog.component.scss'],
})
export class UiConfirmDialogComponent implements OnInit, OnDestroy {
	@HostListener('document:keyup', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		let key = event.key;
		if (key === this.data.keybindConfirm) {
			if (this.data.showOk) {
				this.okClicked();
			} else {
				this.noClicked();
			}
		}
	}

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: UiConfirmDialogData,
		public iconSvc: UiIconService,
		public dialogRef: MatDialogRef<UiConfirmDialogComponent>
	) {
		console.log('CONFIRM DATA', this.data);
	}

	ngOnInit(): void {}

	ngOnDestroy(): void {}

	/**
	 * Emitter za pregled nad potrditvijo
	 */
	onOkClicked = new EventEmitter();
	onNoClicked = new EventEmitter();

	/**
	 *  Če potrdi izbris, vrne emmit
	 */
	okClicked() {
		this.onOkClicked.emit();
		this.dialogRef.close();
	}

	noClicked() {
		this.onNoClicked.emit();
		this.dialogRef.close();
	}
}
