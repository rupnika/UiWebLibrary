export enum Role {
	Guest = 0,
	Member = 1,
	Editor = 2,
	Moderator = 3,
	Admin = 4,
	Poweruser = 5,
	GodMode = 99,
}

export enum HttpStatusCode {
	Ok = 200,
	BadRequest = 400,
	Unauthorized = 401,
	Forbidden = 403,
	NotFound = 404,
	NotAcceptable = 406,
	InternalServerError = 500,
	UnknownError = 0,
}
export enum LogColor {
	Red,
	Green,
	ServerResponse,
	ServerResponseNotOk,
	Blue,
	ServerRequest,
	Yellow,
}

// export enum UiTableFilterType {
// 	Text = 1,
// 	Number,
// 	Date,
// 	Dropdown,
// 	Boolean,
// }
export enum UiTableFilterCondition {
	Equal,
	SmallerThan,
	LargerThan,
}
export enum UiTableCellDataType {
	Text, // Text
	Date,
	DateTime,
	Time,
	Icon,
	Boolean,
	Number,
	Dropdown,
}
export enum UiTableIconAlign {
	Default,
	Center,
	Spread,
	Left,
	Right,
}
export enum UiTableCellContentAlign {
	Default,
	Left,
	Right,
	Center,
}
export enum UiSnackBarStatus {
	Ok = 0,
	Info = 1,
	Warning = 2,
	Error = 3,
	Question = 4,
}
export enum UiDialogPosition {
	Bottom = 0,
	Top = 1,
	Top_Right = 2,
	Top_Left = 3,
	Bottom_Right = 4,
	Bottom_Left = 5,
	Default = 6,
	Under_Border = 7,
}

export enum UiLanguage {
	Slovenian = 'sl',
	English = 'en',
	German = 'de',
}
