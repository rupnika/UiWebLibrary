import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	Renderer2,
	ViewChild,
	ViewEncapsulation,
} from '@angular/core';
import { ControlContainer, FormGroup, FormGroupDirective } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MatSelectChange } from '@angular/material/select';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import dayjs from 'dayjs';
import { Guid } from 'guid-typescript';
import * as _ from 'lodash';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FileHolder } from '../../../entities/misc';
import {
	DROPDOWN_ICON,
	DROPDOWN_SHOW_ICON,
	UiDropdownConfig,
	UiDropdownEntry,
	UiDropdownSelectEmit,
	UiFormError,
} from '../../../entities/ui-controls';
import { Utility } from '../../../entities/utility';
import { UiIconService } from '../../../services/ui-icon.service';
import { UiTextService } from '../../../services/ui-text.service';
import { UiFormInputType } from './../../../entities/ui-controls';
import { SpinnerHttpService } from './../../../http-spinner/services/spinner-http.service';
@Component({
	selector: 'ui-form-input',
	templateUrl: './ui-form-input.component.html',
	styleUrls: ['./ui-form-input.component.scss'],
	encapsulation: ViewEncapsulation.None,
	viewProviders: [{ provide: ControlContainer, useExisting: FormGroupDirective }],
})
export class UiFormInputComponent implements OnInit, OnDestroy {
	@Input() inputType: UiFormInputType = 'input';
	@Input() extraFunctionKeys: string[]; // (+) If user wants extra functionality keys to be used
	// Suffix button
	@Input() hasSuffixButton: boolean = false;

	@Input() suffixFaIcon: IconDefinition;
	@Input() suffixButtonTooltip: string = '';
	@Input() suffixIconTooltip: string = '';
	@Output() suffixButtonClicked = new EventEmitter<any>();
	@Output() suffixIconClicked = new EventEmitter<any>();

	// /SuffixButton
	@Input() prefixText: string;
	@Input() prefixIcon: IconDefinition;
	@Input() suffixIcon: IconDefinition;
	@Input() suffixText: string;
	@Input() suffixTextTooltip: string = '';
	@Input() fileButtonIcon: IconDefinition;
	@Input() labelText: string;
	@Input() placeholder: string;
	@Input() formCtrlName: string;
	@Input() validators: UiFormError[];
	@Input() myForm: FormGroup;
	@Input() isDisabled: boolean = false;
	@Input() isReadonly: boolean = false;
	@Input() isVisible: boolean = true;
	@Input() extraCheckbox: boolean = false;
	@Input() extraCheckboxCtrlName: boolean = false;
	@Input() fieldAppearance: MatFormFieldAppearance = 'outline';
	@Input() cssClass: string;
	@Input() cssClassFormField: string;
	@Input() customWidth: string;
	@Output() onExtraFunctionKeyPress = new EventEmitter<string>();
	@Input() forceUpperCase = false;

	@Input() autoCompleteBtnTitle: string;

	// Textarea
	@Input() textareaMinRows: number = 2;
	// Numeric
	@Input() numericMaxDigits: number = 2;
	@Input() numericMinValue: string;
	@Input() numericMaxValue: string;
	@Input() numericDefaultValue: number;
	// Datetime
	@Input() dateMin: Date = null;
	@Input() dateMax: Date = null;
	@Input() dateInputDisabled: boolean = false;
	@Input() dateInputReadOnly: boolean = false;
	@Output() onDateChanged = new EventEmitter<Date>();
	// Dropdown
	@Input() dropdownConfig: UiDropdownConfig<any>;
	@Output() onDropdownCleared = new EventEmitter<any>();
	@Output() onDropdownSelected = new EventEmitter<any>();
	@Output() onDropdownNewEntry = new EventEmitter<UiDropdownEntry>();

	@Output() onDropdownBtnSelected = new EventEmitter<any>();
	// Dropdown autocomplete
	autocompleteFiltered: Observable<UiDropdownEntry[]>;

	// Checkbox
	@Output() onCheckboxChange = new EventEmitter<boolean>();
	@Output() onExtraCheckboxChange = new EventEmitter<boolean>();
	// File
	@Output() onFileSelected = new EventEmitter<Observable<FileHolder>>();
	@Output() onFilesSelected = new EventEmitter<Observable<FileHolder>>();
	/** '.dll, .txt' */
	@Input() allowedExtensions: string;
	@Input() multipleSelect: string;
	// Generic output
	@Output() onInputChanged = new EventEmitter<string>();
	@Output() onInputNumericValueAboveMax = new EventEmitter<string>();
	@Output() onInputNumericValueBellowMin = new EventEmitter<string>();

	@ViewChild(MatAutocompleteTrigger) matAutocomplete: MatAutocompleteTrigger;

	public faIconInternal: IconDefinition;
	public ctrlObject: any;
	public dropdownInitialized: boolean = false;
	public selectedDropdownItem: UiDropdownEntry;

	subSelectByIndex: Subscription;
	subSelectById: Subscription;
	subSelectByValue: Subscription;
	subDataInitialized: Subscription;
	subDropdownCleared: Subscription;

	constructor(
		public el: ElementRef,
		public ren: Renderer2,
		public iconSvc: UiIconService,
		public textSvc: UiTextService,
		public spinnerHttp: SpinnerHttpService
	) {}

	ngOnDestroy(): void {
		if (this.inputType === 'dropdown') {
			this.subSelectByIndex?.unsubscribe();
			this.subSelectById?.unsubscribe();
			this.subSelectByValue?.unsubscribe();
			this.subDataInitialized?.unsubscribe();
			this.subDropdownCleared?.unsubscribe();
		}
	}

	_onClearDropdownClicked(event) {
		event.stopPropagation();
		this.selectedDropdownItem = null;
		this.myForm.get(this.formCtrlName).reset();
		this.myForm.markAllAsTouched();
		this.onDropdownCleared.emit();
	}
	ngOnInit(): void {
		this.faIconInternal = this.suffixFaIcon;
		// if (this.cssClass) {
		// 	this.ren.addClass(this.el.nativeElement, this.cssClass);
		// }
		if (this.formCtrlName) {
			this.ctrlObject = this.myForm.controls[this.formCtrlName];
		}

		if (this.inputType === 'dropdown') {
			this.subDataInitialized = this.dropdownConfig.onDataInitialized.subscribe(() => {
				this.initDropdown();
			});
			this.subDropdownCleared = this.dropdownConfig.onClearDropdown.subscribe(() => {
				this.myForm.get(this.formCtrlName).reset();
			});
			this.subSelectByIndex = this.dropdownConfig.onSelectByIndex.subscribe(
				(data: UiDropdownSelectEmit<number>) => {
					const entryToReturn = this.dropdownConfig.data[data.value];
					const entryToSelect = this.dropdownConfig.dataInternal[data.value];
					if (!_.isNil(entryToSelect)) {
						this.myForm.get(this.formCtrlName).setValue(entryToSelect);

						if (data.emitChange) {
							this.onDropdownSelected.emit(entryToReturn);
						}
						this.matAutocomplete?.closePanel();
					}
				}
			);
			this.subSelectById = this.dropdownConfig.onSelectById.subscribe((data: UiDropdownSelectEmit<any>) => {
				const entryToSelect = this.dropdownConfig.getDataInternalById(data.value);
				const entryToReturn = this.dropdownConfig.getDataById(data.value);

				if (!_.isNil(entryToSelect)) {
					this.myForm.get(this.formCtrlName).setValue(entryToSelect);
					if (data.emitChange) {
						this.onDropdownSelected.emit(entryToReturn);
					}

					this.matAutocomplete?.closePanel();
				}
			});
			this.subSelectByValue = this.dropdownConfig.onSelectByValue.subscribe(
				(data: UiDropdownSelectEmit<string>) => {
					// const entryToSelect = this.dropdownConfig.data.find((x) => {
					const entryToSelect = this.dropdownConfig.getDataInternalByValue(data.value);
					const entryToReturn = this.dropdownConfig.getDataByValue(data.value);

					if (!_.isNil(entryToSelect)) {
						this.myForm.get(this.formCtrlName).setValue(entryToSelect);

						if (data.emitChange) {
							this.onDropdownSelected.emit(entryToReturn);
						}
						this.matAutocomplete?.closePanel();
					}
				}
			);
		}
	}
	ngAfterViewInit() {
		this.ren.addClass(this.el.nativeElement, 'ui-form-input');
		if (this.inputType === 'checkbox') {
			this.ren.addClass(this.el.nativeElement, 'ui-checkbox');
		}
		if (this.cssClass) {
			this.ren.addClass(this.el.nativeElement, this.cssClass);
		}
	}

	onClickInternal() {
		if (!this.isDisabled) {
			this.suffixButtonClicked.emit();
		}
	}
	isDropdownDisabled(isHttpActive: boolean) {
		return isHttpActive || this.isDisabled;
	}

	hasSuffixText() {
		return !_.isNil(this.suffixText);
	}

	/**</string></any>
	 * Main entry point when input changed. Input can be any supported type (see UiFormInputType)
	 * @param event
	 */
	public _onInputChanged(event) {
		if (this.inputType === 'number') {
			this.onNumericInputChanged(event);
		} else if (this.inputType === 'file' || this.inputType === 'fileButton') {
			this._onFileSelected(event);
		} else if (this.inputType === 'datetime') {
			this._onDateChanged(event);
		} else {
			const val = event.target.value;
			this.onInputChanged.emit(val);
		}
	}

	public _onSuffixIconClicked() {
		this.suffixIconClicked.emit();
	}
	//#region ========== Input validation ==========
	public hasError() {
		const err = this.getError();

		if (err) {
			return true;
		}
		return false;
	}
	private getError(): UiFormError[] {
		if (!this.validators || this.validators.length === 0) {
			return null;
		}

		const errors: UiFormError[] = [];
		for (let i = 0; i < this.validators.length; i++) {
			const e = this.validators[i];
			const has = this.myForm.controls[this.formCtrlName].hasError(e.rule.toLowerCase());

			if (this.inputType === 'datetime') {
			}
			if (has) {
				errors.push(e);
			}
		}
		return errors.length == 0 ? null : errors;
	}

	public getErrorMessage() {
		const err = this.getError();
		if (err) {
			return err
				.map((value, index, arr) => {
					return value.message;
				})
				.join(' ');
		}
		return 'N/A';
	}
	//#endregion

	//#region ========== Styling ==========
	getCustomStyle() {
		let style = {
			width: '',
		};

		if (this.customWidth) {
			style.width = `${this.customWidth}`;
			return style;
		}

		return '';
	}
	//#endregion

	//#region ========== Numeric input ==========

	@ViewChild('numberInput') numberInput: ElementRef;

	// Allow decimal numbers and negative values
	private regex: RegExp = new RegExp(/^-?\d*[\.\,]?\d*$/g);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = [
		'Backspace',
		'Tab',
		'End',
		'Home',
		'+',
		'*',
		'/',
		'ArrowLeft',
		'ArrowRight',
		'Del',
		'Delete',
	];

	private onNumericInputChanged(event) {
		const val = event.target.value;
		// decimal separator shoud be . (dot)
		let newVal = '';
		let rawVal = '';

		let isOnInputNumericValueAboveMax = false;
		let isOnInputNumericValueBellowMin = false;

		let oldVal = val;

		// valid number var entered into textbox
		if (val !== '') {
			let valNumber = parseFloat(val.replace(',', '.'));
			oldVal = valNumber.toFixed(this.numericMaxDigits);

			const min = parseFloat(this.numericMinValue);
			const max = parseFloat(this.numericMaxValue);
			if (this.numericMinValue && valNumber < min) {
				valNumber = min;
				isOnInputNumericValueBellowMin = true;
			}
			if (this.numericMaxValue && valNumber > max) {
				valNumber = max;
				isOnInputNumericValueAboveMax = true;
			}
			rawVal = valNumber.toFixed(this.numericMaxDigits);
		} else if (this.numericDefaultValue) {
			rawVal = parseFloat(this.numericDefaultValue.toString()).toFixed(this.numericMaxDigits);
		} else {
			rawVal = parseFloat('0').toFixed(this.numericMaxDigits);
		}

		const fixedValue = rawVal.replace('.', this.textSvc.digitsSeparator);
		this.numberInput.nativeElement.value = fixedValue;
		// const tmpPatch = parseFloat(rawVal).toFixed(this.numericMaxDigits);
		// const patchValue = parseFloat(tmpPatch);
		this.myForm.get(this.formCtrlName).patchValue(fixedValue);
		this.onInputChanged.emit(newVal);

		if (isOnInputNumericValueBellowMin === true) {
			this.onInputNumericValueBellowMin.emit(oldVal);
		}

		if (isOnInputNumericValueAboveMax === true) {
			this.onInputNumericValueAboveMax.emit(oldVal);
		}
	}

	public onNumericInput_KeyDown(event: KeyboardEvent) {
		// Allow Backspace, tab, end, and home keys
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		if (this.extraFunctionKeys?.indexOf(event.key) !== -1) {
			// if pressed key is in user defined function key...emit to all subscribers
			this.onExtraFunctionKeyPress.emit(event.key);
		}

		let current: string = this.numberInput.nativeElement.value;
		const position = this.numberInput.nativeElement.selectionStart;
		const next: string = [
			current.slice(0, position),
			event.key == 'Decimal' ? '.' : event.key,
			current.slice(position),
		].join('');

		if (next && !String(next).match(this.regex)) {
			console.log('PREVENT', next);

			event.preventDefault();
		}
	}

	//#endregion

	//#region ========== File input ==========
	@ViewChild('file') file;
	@ViewChild('fileButton') fileButton;

	_onOpenFileDialog() {
		this.file.nativeElement.click();
	}
	_onOpenFileButtonDialog() {
		this.fileButton.nativeElement.click();
	}
	_onFileSelected(event) {
		const selectedFiles = <File[]>event.target.files;
		this.onFileSelected.emit(this.loadSelectedFiles(selectedFiles));
	}

	private loadSelectedFiles(files: File[]): Observable<FileHolder> {
		let loadedFiles = [];

		return new Observable((obv: Subscriber<FileHolder>) => {
			for (let i = 0; i < files.length; i++) {
				let file = files[i];
				file.arrayBuffer().then((arrayBuffer) => {
					const b64 = Utility.arrayBufferToBase64(arrayBuffer);
					const newFile = new FileHolder();
					newFile.FileBase64 = b64;
					newFile.FileName = file.name;
					newFile.FileType = file.type;
					newFile.Size = Utility.formatSizeUnits(file.size);
					newFile.Size_b = file.size;
					newFile.Size_kb = parseFloat((file.size / 1000).toFixed(2));
					newFile.EditedDate = this.timeConverter(file.lastModified);
					loadedFiles.push(newFile);
					obv.next(newFile);
					// send complete() only when all files are loaded (if multiple files selected)
					if (loadedFiles.length === files.length) {
						obv.complete();
					}
				});
			}
		});
	}

	private timeConverter(UNIX_timestamp) {
		var editDate = new Date(UNIX_timestamp);
		var time = dayjs(editDate).format('DD.MM.YYYY');
		return time;
	}

	private loadSelectedFile(file: File): Observable<FileHolder> {
		var reader = new FileReader();
		reader.readAsDataURL(file);

		return Observable.create((obv: Subscriber<any>) => {
			reader.onload = (_event) => {
				let fHold = new FileHolder();
				fHold.FileName = file.name;
				fHold.FileBase64 = <string>reader.result;
				obv.next(fHold);
			};
		});
	}
	//#endregion

	//#region ========== Checkbox input ==========
	_onCheckboxChange(event: MatCheckboxChange) {
		this.onCheckboxChange.emit(event.checked);
	}

	_onExtraCheckboxChange(event: MatCheckboxChange) {
		this.onExtraCheckboxChange.emit(event.checked);
	}
	//#endregion

	//#region ========== Datetime input ==========
	_onDateChanged(event: MatDatepickerInputEvent<Date>) {
		this.onDateChanged.emit(event.value);
	}
	//#endregion

	//#region ========== Dropdown input ==========
	initDropdown() {
		console.log('Init dropdown', this.dropdownConfig);

		if (this.dropdownConfig.autocomplete) {
			this.dropdownShowNewEntry = this.dropdownConfig.allowNewEntry || false;
			this.dropdownCtrl = this.myForm.get(this.formCtrlName);
			this.autocompleteFiltered = this.dropdownCtrl.valueChanges.pipe(
				startWith(''),
				map((value: any) => (!_.isNil(value) ? (typeof value === 'string' ? value : value.value) : '')),
				map((name: string) => (name ? this.filterAutocomplete(name) : this.dropdownConfig.dataInternal.slice()))
			);
		}
		setInterval(() => {
			this.dropdownInitialized = true;
		}, 10);
	}
	_onDropdownSelected(event: MatSelectChange) {
		const entry: UiDropdownEntry = event.value;
		const selected = this.dropdownConfig.data.find((x) => {
			if (x[this.dropdownConfig.valueField] === entry.value) {
				return x;
			}
		});

		this.onDropdownSelected.emit(selected);
	}

	showDropdownIcon(e: UiDropdownEntry): boolean {
		const origData = this.dropdownConfig.getDataByValue(e.value);

		if (_.isNil(origData)) {
			return false;
		}

		return !_.isNil(origData[DROPDOWN_SHOW_ICON]);
	}
	getDropdownIcon(e: UiDropdownEntry): IconDefinition {
		const origData = this.dropdownConfig.getDataByValue(e.value);

		if (_.isNil(origData)) {
			return;
		}

		return origData[DROPDOWN_ICON];
	}
	@ViewChild('auto') autoComplete;
	getAutocompleteTooltipText() {
		return this.selectedDropdownItem?.value;
	}

	getDropdownTooltipText(e: UiDropdownEntry) {
		if (this.showDropdownIcon(e)) {
			return this.textSvc.getText('di_prio_2');
		} else {
			return '';
		}
	}
	//#endregion

	//#region ========== Dropdown autocomplete input ==========

	autocompleteHandleChange(event) {
		if (event.target.value === '') {
			this.onDropdownCleared.emit();
		}
		return;
	}
	_dropdownAutocompleteSelected(event) {
		const entry: UiDropdownEntry = event.value;
		this.selectedDropdownItem = entry;

		console.log('ENTRY', entry);
		if (entry.value.indexOf(this.prompt) === 0) {
			this.onAddNewEntry();
		}
		const selected = this.dropdownConfig.data.find((x) => {
			const type = typeof x;

			if (type === 'string' && x === entry.value) {
				return x;
			} else if (type === 'object' && x[this.dropdownConfig.valueField] === entry.value) {
				return x;
			}
		});
		this.onDropdownSelected.emit(selected);
	}
	onAutocompleteDropdownOpened() {
		this.initDropdown();
	}
	displayFn(data: UiDropdownEntry): string {
		return data && data.value ? data.value : '';
	}
	prompt = 'Press <enter> to add "';
	dropdownShowNewEntry: boolean = false;
	dropdownCtrl: any;

	onButtonSelect() {
		this.onDropdownBtnSelected.emit();
	}

	onAddNewEntry() {
		if (_.isNil(this.dropdownConfig.allowNewEntry) || !this.dropdownConfig.allowNewEntry) {
			return;
		}
		let option = this.removePromptFromOption(this.dropdownCtrl.value);
		if (!this.dropdownConfig.dataInternal.some((entry) => entry.value.toLowerCase() === option.toLowerCase())) {
			let tmp = new UiDropdownEntry();
			tmp.id = `option-${Guid.create()}`;
			tmp.value = option;
			// const index = this.dropdownConfig.data.push(tmp) - 1;
			// this.dropdownCtrl.setValue(this.dropdownConfig.data[index]);
			this.onDropdownNewEntry.emit(tmp);
		}
	}
	removePromptFromOption(option) {
		if (option.startsWith(this.prompt)) {
			option = option.substring(this.prompt.length, option.length - 1);
		}
		return option;
	}
	filterAutocomplete(value: string): any[] {
		// get only options that contains 'value'
		let filteredData = this.dropdownConfig.dataInternal.filter((data) => {
			let index = -1;
			const dataDisplay: string = data.value.toLowerCase();
			index = dataDisplay.indexOf(value.toLowerCase());
			if (index > -1) return data;
		});

		this.dropdownShowNewEntry = filteredData?.length === 0 && this.dropdownConfig.allowNewEntry;
		//
		if (this.dropdownShowNewEntry) {
			let newItem = new UiDropdownEntry();
			newItem.id = 'temp_id';
			newItem.value = this.prompt + value + '"';
			filteredData.push(newItem);
		}

		return filteredData;
	}
	//#endregion

	@ViewChild('textInput') textInput: ElementRef;
	public getTextValue() {
		const val = this.textInput.nativeElement.value;
		return val;
	}
}
