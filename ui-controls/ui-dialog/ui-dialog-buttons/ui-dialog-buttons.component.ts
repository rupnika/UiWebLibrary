import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'ui-dialog-buttons',
	templateUrl: './ui-dialog-buttons.component.html',
	styleUrls: ['./ui-dialog-buttons.component.scss'],
	encapsulation: ViewEncapsulation.None,
	host: { class: 'ui-dialog-buttons' },
})
export class UiDialogButtonsComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}
}
