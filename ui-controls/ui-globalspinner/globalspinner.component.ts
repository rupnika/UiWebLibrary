import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';

@Component({
	selector: 'app-globalspinner',
	templateUrl: './globalspinner.component.html',
	styleUrls: ['./globalspinner.component.scss'],
})
export class UiGlobalSpinnerComponent implements OnInit {
	constructor() {}

	color: ThemePalette = 'accent';
	mode: ProgressSpinnerMode = 'indeterminate';

	ngOnInit(): void {}
}
