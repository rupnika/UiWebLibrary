import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class UiLoaderService {
	constructor() {}

	isProcessing = new Subject<boolean>();
	show() {
		this.isProcessing.next(true);
	}
	hide() {
		this.isProcessing.next(false);
	}
}
