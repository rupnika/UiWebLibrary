import { MatPaginatorIntl } from '@angular/material/paginator';

const defaultRangeLabel = (page: number, pageSize: number, length: number) => {
	if (length == 0 || pageSize == 0) {
		return ``;
	}

	length = Math.max(length, 0);

	const startIndex = page * pageSize;

	// If the start index exceeds the list length, do not try and fix the end index to the end.
	const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

	return `${startIndex + 1} - ${endIndex} / ${length}`;
};

export function getPaginatorTranslations() {
	const paginatorIntl = new MatPaginatorIntl();
	// const textSvc = new UiTextService(UiLanguage.Slovenian);
	paginatorIntl.itemsPerPageLabel = ''; //textSvc.getText('paginator_per_page'); //'Zapisov na stran:';
	paginatorIntl.firstPageLabel = ''; //textSvc.getText('paginator_first_page'); //'Prva stran';
	paginatorIntl.lastPageLabel = ''; //textSvc.getText('paginator_last_page'); //'Zadnja stran';
	paginatorIntl.nextPageLabel = ''; //textSvc.getText('paginator_next_page'); //'Naslednja stran';
	paginatorIntl.previousPageLabel = ''; //textSvc.getText('paginator_previous_page'); //'Prejšnja stran';
	paginatorIntl.getRangeLabel = defaultRangeLabel;

	return paginatorIntl;
}
