import { SelectionModel } from '@angular/cdk/collections';
import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	HostBinding,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	Renderer2,
	SimpleChanges,
	ViewChild,
} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { MatSort, MatSortable, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import * as saveAs from 'file-saver';
import { Guid } from 'guid-typescript';
import * as _ from 'lodash';
import { isNumber } from 'projects/Admin/src/app/shared/common/byte.pipe';
import { Subscription } from 'rxjs';
import { UiTableCellContentAlign, UiTableCellDataType, UiTableIconAlign } from '../../entities/enums';
import {
	INLINE_EDIT_ENABLED,
	NewTabClickedVm,
	TABLE_IS_SELECTED,
	TABLE_ROWID,
	TABLE_ROWID_PARENT,
	TABLE_ROW_HASCHILDS,
	TABLE_ROW_ISCHILD,
	TABLE_ROW_ISDISABLED,
	TABLE_ROW_ISVISIBLE,
	UiDataInitialized,
	UiTableCache,
	UiTableCellClicked,
	UiTableCellDropdownChanged,
	UiTableCellIconConfiguration,
	UiTableColumn,
	UiTableConiguration,
	UiTableExcludeOrIncludeChanged,
	UiTableFilterArgs,
	UiTableFilters,
} from '../../entities/ui-controls';
import { UiIconService } from '../../services/ui-icon.service';
import { UiTextService } from '../../services/ui-text.service';
import { TABLE_ROW_ISEXPANDED, TABLE_ROW_ISPARENT, TABLE_ROW_ORIG } from './../../entities/ui-controls';

@Component({
	selector: 'ui-table',
	templateUrl: './ui-table.component.html',
	styleUrls: ['./ui-table.component.scss'],
	host: { class: 'ui-table' },
	changeDetection: ChangeDetectionStrategy.Default,
})
export class UiTableComponent<T, K = void> implements OnInit, OnDestroy, OnChanges {
	@ViewChild('table') table: MatTable<Element>;
	@ViewChild('tableRef') tableRef: ElementRef;
	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort: MatSort;

	@Input() cssClassCustom: string;
	// Inputs
	@Input() uiTableConfig: UiTableConiguration<T, K>;
	@Input() uiClass: string;
	@Input() isVisible: boolean = true;
	//@Input() uiWidth: string;
	@Input() uiShowBorder: boolean = true;
	@Input() uiBlackBorder: boolean = false;
	@Input() uiRowMinHeight: string;

	@Input() tableOverflowX: string = 'auto';
	@Input() tableOverflowY: string = 'hidden';

	// Outputs
	@Output() onRowClick = new EventEmitter<T>();
	@Output() onRowDoubleClick = new EventEmitter<T>();
	@Output() onCheckboxSelectionChanged = new EventEmitter<T[]>();
	@Output() onCellDropdownChanged = new EventEmitter<UiTableCellDropdownChanged>();

	@Output() onFiltersExported = new EventEmitter<string>();
	@Output() onFiltersImported = new EventEmitter();

	@Output() onSortingChanged = new EventEmitter<Sort>();

	@Output() onCellClicked = new EventEmitter<UiTableCellClicked>();
	@Output() onCellDblClicked = new EventEmitter<UiTableCellClicked>();
	@Output() onClearFilter = new EventEmitter<string>();

	@Output() onPageChange = new EventEmitter<PageEvent>();

	@HostBinding('style.display') public visible: string = '';

	//include or exclude all
	@Output() onIncludeOrExludeSelectionChanged = new EventEmitter<UiTableExcludeOrIncludeChanged>();

	@Output() onRightMouseClick = new EventEmitter<NewTabClickedVm>();
	@Output() onMiddleMouseClick = new EventEmitter<NewTabClickedVm>();

	public showFilterRow: boolean = false;
	public templateFor: string;
	public isFilterActive: boolean;
	public displayedColumns: string[];
	public displayedColumnsFilter: string[];
	public visibleColumns: UiTableColumn[] = [];
	public _inlineInputWidth: number;
	public _inlineInputParentWidth: number;

	private isSingleClick: Boolean = true;
	private activeFilters: UiTableFilterArgs[] = [];
	// Allow decimal numbers and negative values
	private regex: RegExp = new RegExp(/^-?\d*[\.\,]?\d*$/g);
	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = [
		'Backspace',
		'Tab',
		'End',
		'Home',
		'+',
		'*',
		'/',
		'ArrowLeft',
		'ArrowRight',
		'Del',
		'Delete',
	];

	_subDatainitialized: Subscription;
	_subOnSelectCheckboxes: Subscription;
	_subOnSelectCheckboxesChilds: Subscription;
	_subOnExpandAllParents: Subscription;
	_subOnClearCheckboxes: Subscription;
	_subOnCollapseAllParents: Subscription;
	_visibilitySub: Subscription;
	_subOnSort: Subscription;
	_subImportTableFilters: Subscription;
	_onToggleVisibility: Subscription;
	_mouseLocation: { x: number; y: number } = { x: 0, y: 0 };

	sortStart: string = 'asc';
	CACHE: UiTableCache;

	constructor(
		public iconSvc: UiIconService,
		public textSvc: UiTextService,
		private cdr: ChangeDetectorRef,
		public ren: Renderer2
	) {
		this.CACHE = new UiTableCache();
		this.CACHE.cached = [];
	}

	hasDefinedMaxHeight() {
		return !_.isNil(this.uiTableConfig.table.maxHeightPx);
	}

	ngOnChanges(changes: SimpleChanges) {
		if (!this.isVisible) {
			this.visible = 'none';
		} else {
			this.visible = '';
		}
	}

	ngOnDestroy(): void {
		this._subDatainitialized.unsubscribe();
		this._subOnSelectCheckboxes.unsubscribe();
		this._subOnSelectCheckboxesChilds.unsubscribe();
		this._subImportTableFilters.unsubscribe();
		this._subOnSort.unsubscribe();
		this._onToggleVisibility.unsubscribe();

		if (this.uiTableConfig.tableTree?.enabled) {
			this._subOnExpandAllParents.unsubscribe();
			this._subOnCollapseAllParents.unsubscribe();
		}
		if (this._subOnClearCheckboxes) {
			this._subOnClearCheckboxes.unsubscribe();
		}
		this.CACHE = null;
	}

	preloadTableData() {}

	ngOnInit() {
		if (this.uiTableConfig.dataSource == null) {
			this.uiTableConfig.dataSource = new MatTableDataSource<T>();
		}
		this.uiTableConfig.table.width = this.uiTableConfig.table.width || '100%';
		this._onToggleVisibility = this.uiTableConfig.table.onToggleVisibility.subscribe((isVisible: boolean) => {
			if (isVisible) {
				this.ren.removeClass(this.tableRef.nativeElement, 'hide-div');
			} else {
				this.ren.addClass(this.tableRef.nativeElement, 'hide-div');
			}
		});
		this.uiTableConfig.table.isExportVisible = _.isNil(this.uiTableConfig.table.isExportVisible)
			? false
			: this.uiTableConfig.table.isExportVisible;

		this.initColumns();

		this.uiTableConfig.paginator.pageSizeOptions = this.uiTableConfig.paginator.pageSizeOptions || [
			5, 10, 15, 20, 25, 50, 100, 500,
		];

		this._subOnSort = this.uiTableConfig.onSort.subscribe((sort: Sort) => {
			this._onSort(sort);
		});
		this._subDatainitialized = this.uiTableConfig.onDataInitialized.subscribe((data: UiDataInitialized) => {
			// Update the columns aswell incase they changed in table config
			this.initColumns();
			if (!_.isNil(data.activeFilters)) {
				this.activeFilters = JSON.parse(data.activeFilters.filters);
				this._reApplyFilters();
			} else {
				this._reApplyFilters();
			}

			this.initPaginator();

			this.preloadTableData();
			this.uiTableConfig.dataSource.data = data.data;
			// const d = {
			// 	active: this.uiTableConfig.dataSource.sort.active,
			// 	dir: this.uiTableConfig.dataSource.sort.direction,
			// };
			//if activeSorting apply sort
			// console.log('TABLE INIT CALLED');
			// console.log('Current table sort', d);
			// console.log('Incoming sort', data.activeSort);

			// apply new sorting only if sorting has changed
			if (
				!_.isNil(data.activeSort) &&
				this.uiTableConfig.dataSource.sort.active !== data.activeSort.active &&
				this.uiTableConfig.dataSource.sort.direction !== data.activeSort.direction
			) {
				this._onSort(data.activeSort);
				// const d2 = {
				// 	active: this.uiTableConfig.dataSource.sort.active,
				// 	dir: this.uiTableConfig.dataSource.sort.direction,
				// };
				// console.log('TABLE SORTED', d2);
			}
		});

		this._subImportTableFilters = this.uiTableConfig.onImportFilters.subscribe((data: UiTableFilters) => {
			this.onFiltersImported.emit(this.importActiveFilters(data));
		});

		this._subOnSelectCheckboxes = this.uiTableConfig.onSelectCheckboxes.subscribe((data: T[]) => {
			this.checkboxSelection.select(...data);
			const selectedRows = this.checkboxSelection.selected.map((x) => <T>x);
			this.uiTableConfig.checkboxSelectedRows = selectedRows;
		});

		this._subOnSelectCheckboxesChilds = this.uiTableConfig.onSelectCheckboxesChilds.subscribe((data: K[]) => {
			let internalChildRow: T[] = [];
			this.uiTableConfig.dataSource.data.forEach((x) => {
				data.forEach((c) => {
					const origRow: K = <K>x[TABLE_ROW_ORIG];

					if (JSON.stringify(origRow) === JSON.stringify(c)) {
						internalChildRow.push(x);
					}
				});
			});

			// internal row object selection
			this.checkboxSelection.select(...internalChildRow);
			const selectedRows = this.checkboxSelection.selected.map((x) => <T>x);
			this.uiTableConfig.checkboxSelectedRows = selectedRows;
		});

		// Only subscribe to expand/collapse events if TableTree is enabled
		if (this.uiTableConfig.tableTree?.enabled) {
			this._subOnExpandAllParents = this.uiTableConfig.tableTree?.onExpandAllParents.subscribe(() => {
				this.toggleAllParents(true);
			});
			this._subOnCollapseAllParents = this.uiTableConfig.tableTree?.onCollapseAllParents.subscribe(() => {
				this.toggleAllParents(false);
			});
		}

		this._subOnClearCheckboxes = this.uiTableConfig.checkbox?.onClearCheckboxes.subscribe(() => {
			this.checkboxSelection.clear();
			this.uiTableConfig.checkboxSelectedRows = [];
		});

		document.onmousemove = (e) => {
			this._mouseLocation.x = e.clientX;
			this._mouseLocation.y = e.clientY;
		};
	}

	public changeVisibility() {
		if (!this.isVisible) {
			this.visible = 'none';
		} else {
			this.visible = '';
		}
	}

	private toggleAllParents(expand: boolean) {
		this.uiTableConfig.dataSource.data.forEach((x) => {
			if (this.isParentRow(x)) {
				x[TABLE_ROW_ISEXPANDED] = expand;
				let allChilds = this.uiTableConfig.dataSource.data.filter(
					(c) => c[TABLE_ROWID_PARENT] === x[TABLE_ROWID]
				);
				allChilds.forEach((c) => (c[TABLE_ROW_ISVISIBLE] = expand));
			}
		});
	}
	private initColumns() {
		// Set the default values for columns
		this.uiTableConfig.columns.forEach((col: UiTableColumn) => {
			this._initColumDefaultValues(col);
		});

		this.visibleColumns = this.uiTableConfig.columns.filter((c) => {
			if (c.isVisible) {
				return c;
			}
		});

		// add checkbox column
		this.displayedColumns = [];
		if (this.uiTableConfig.table?.enableRowCheckbox) {
			this.displayedColumns.push('checkboxSelect');
		}
		this.visibleColumns.forEach((x) => this.displayedColumns.push(x.fieldName));

		this.displayedColumnsFilter = this.visibleColumns.map((c) => c.fieldName + '_filter');
	}
	private _initColumDefaultValues(col: UiTableColumn) {
		col.id = Guid.create().toString();
		col.sortingEnabled = _.isNil(col.sortingEnabled) ? true : col.sortingEnabled;
		col.isVisible = _.isNil(col.isVisible) ? true : col.isVisible;
		col.registerClick = _.isNil(col.registerClick) ? true : col.registerClick;
		col.cellDataType = _.isNil(col.cellDataType) ? UiTableCellDataType.Text : col.cellDataType;
		col.filterEnabled = _.isNil(col.filterEnabled) ? true : col.filterEnabled;
		col.showOnMobile = _.isNil(col.showOnMobile) ? true : col.showOnMobile;
		col.showText = _.isNil(col.showText) ? true : col.showText;
		col.showDateInLocalTime = _.isNil(col.showDateInLocalTime) ? true : col.showDateInLocalTime;
		//col.inlineEditEnabled = _.isNil(col.inlineEditEnabled) ? false : col.inlineEditEnabled;

		if (_.isNil(col.filterDataType)) {
			col.filterDataType = col.cellDataType;
		}

		if (col.childColumn && !col.childColumn.cellDataType) {
			col.childColumn.cellDataType = UiTableCellDataType.Text;
		}
		// set default value for icons
		if (col.cellIcons) {
			// col.cellIcons.forEach((icon: UiTableCellIconConfiguration) => {
			// 	icon.isVisible = _.isNil(icon.isVisible) ? true : icon.isVisible;
			// });
		} else {
			col.cellIcons = [];
		}
	}
	private initPaginator() {
		this.uiTableConfig.dataSource.sort = this.sort;
		this.sortStart = this.uiTableConfig.matSortStart;

		if (this.uiTableConfig.paginator.showPaginator) {
			// setTimeout(() => {
			this.uiTableConfig.dataSource.paginator = this.paginator;
			// this.uiTableConfig.paginator.pageSizeOptions = this.uiTableConfig.paginator.pageSizeOptions || [
			// 	5,
			// 	10,
			// 	15,
			// 	20,
			// 	25,
			// 	50,
			// 	100,
			// 	500,
			// ];
			this.uiTableConfig.paginator.pageSize = this.uiTableConfig.paginator.pageSize || 10;

			this.visibleColumns.forEach((col) => {
				col.fieldNameFilter = col.fieldName + '_filter';

				// filtering and sorting is enabled if not provided in configuration
				col.filterEnabled = _.isNil(col.filterEnabled) ? true : col.filterEnabled;
				col.sortingEnabled = _.isNil(col.sortingEnabled) ? false : col.sortingEnabled;
			});
			// }, 1);
		}
	}
	public isIncludeExcludeAllEnabled(column: UiTableColumn) {
		return column.includeExcludeEnabled;
	}
	public isFilterEnabled(column: UiTableColumn) {
		return (
			(column.filterDataType == UiTableCellDataType.Text ||
				column.filterDataType == UiTableCellDataType.Boolean ||
				column.filterDataType == UiTableCellDataType.Dropdown) &&
			column.filterEnabled &&
			this.uiTableConfig.dataSource.data.length > 1
		);
	}
	public isTextDataType(col: UiTableColumn) {
		let cellDataType =
			col.cellDataType === UiTableCellDataType.Text ||
			col.cellDataType === UiTableCellDataType.Boolean ||
			col.cellDataType === UiTableCellDataType.Icon ||
			col.cellDataType === UiTableCellDataType.Number;

		// check if its child column
		if (!_.isNil(col.childColumn)) {
			cellDataType =
				_.isNil(col.childColumn.cellDataType) ||
				col.childColumn.cellDataType === UiTableCellDataType.Text ||
				col.childColumn.cellDataType === UiTableCellDataType.Boolean ||
				col.childColumn.cellDataType === UiTableCellDataType.Icon ||
				col.childColumn.cellDataType === UiTableCellDataType.Number;
		}
		return cellDataType;
	}
	public isIconDataType(col: UiTableColumn) {
		return col.cellDataType === UiTableCellDataType.Icon;
	}
	public isDateDataType(col: UiTableColumn) {
		return col.cellDataType === UiTableCellDataType.Date;
	}
	public isDateTimeDataType(col: UiTableColumn) {
		return col.cellDataType === UiTableCellDataType.DateTime;
	}
	public isDropdownDataType(col: UiTableColumn) {
		return col.cellDataType === UiTableCellDataType.Dropdown;
	}
	public showCellText(col: UiTableColumn) {
		return !col.cellText && col.cellDataType !== UiTableCellDataType.Icon;
	}
	public isIconVisible(icon: UiTableCellIconConfiguration, row) {
		const isVisible = _.isNil(icon.isVisible) ? true : icon.isVisible(row);
		const hasIcon = _.isNil(icon.getIcon) ? false : !_.isNil(icon.getIcon(row));
		return isVisible && hasIcon;
	}
	public showSorting(col: UiTableColumn) {
		// Only icons should now be sortable
		if (col?.cellDataType === UiTableCellDataType.Icon) {
			return false;
		}
		return col?.sortingEnabled;
	}
	public shouldHighlightCellOnHover(col: UiTableColumn, row) {
		if (row[TABLE_ROW_ISDISABLED]) {
			return false;
		}
		if (_.isNil(col.highlightOnHover)) {
			return true;
		}

		const res = col.highlightOnHover(row);
		return res;
	}
	public showTableTreeToggleIcon(isFirstCol: boolean, col: UiTableColumn, row: T) {
		if (isFirstCol && this.uiTableConfig.tableTree?.enabled) {
			return row[TABLE_ROW_HASCHILDS];
		}

		return false;
	}

	//#region ========== TABLE EVENTS ==========
	onRightClick(row: T, icon: UiTableCellIconConfiguration) {
		// open in new tab when click right mouse btn and open new tab menu item
		let model = new NewTabClickedVm();
		model.icon = icon;
		model.row = row;
		this.onRightMouseClick.emit(model);
	}
	onClickRowIcon(row: T, event: MouseEvent, icon: UiTableCellIconConfiguration) {
		// open in new tab when click + ctrl or middle mouse btn
		if (event.button === 1) {
			let model = new NewTabClickedVm();
			model.icon = icon;
			model.row = row;
			this.onMiddleMouseClick.emit(model);
		}
	}
	paginatorChangeEvent(event?: PageEvent) {
		this.onPageChange.emit(event);
	}
	public _onRowClick(e, col: UiTableColumn, row, isFirstCol: boolean) {
		e.stopPropagation();

		if (row[TABLE_ROW_ISDISABLED]) {
			return;
		}

		if (!col.registerClick) {
			return;
		}

		// dont process if clicked cell is first and has childs
		if (
			isFirstCol &&
			this.uiTableConfig.tableTree.enabled &&
			!_.isNil(row[TABLE_ROW_HASCHILDS]) &&
			row[TABLE_ROW_HASCHILDS]
		) {
			return;
		}

		this.isSingleClick = true;

		if (!this.shouldHighlightCellOnHover(col, row)) {
			return;
		}

		// Inline edit logic
		this.uiTableConfig.dataSource.data.forEach((r) => {
			r[INLINE_EDIT_ENABLED] = false;
		});

		let inlineEnabled = !_.isNil(col.inlineEditEnabled) && col.inlineEditEnabled(row) && col.singleClickInlineEdit;

		// enable inline editing for TableTree child row
		if (this.uiTableConfig.tableTree.enabled && !_.isNil(col.childColumn)) {
			inlineEnabled =
				!_.isNil(col.childColumn?.inlineEditEnabled) &&
				col.childColumn.inlineEditEnabled(row) &&
				col.childColumn.singleClickInlineEdit;
		}

		row[INLINE_EDIT_ENABLED] = inlineEnabled;

		this.uiTableConfig.columns.forEach((x) => {
			x.showInline = x.id === col.id;
		});
		// wait 150ms before register click as single click
		// click before 150ms is registered as double click
		this.highlightRow();
		if (this.isSingleClick) {
			if (this.uiTableConfig.table?.highlightSelectedRow) {
				this.uiTableConfig.dataSource.data.forEach((r) => {
					const oldVal = r[INLINE_EDIT_ENABLED];

					// INLINE EDIT - handle if same cell is clicked
					const isInlineSameCell = oldVal && r === row;
					if (!isInlineSameCell) {
						r[INLINE_EDIT_ENABLED] = false;
					}
					r[TABLE_IS_SELECTED] = false;
				});
				row[TABLE_IS_SELECTED] = true;
			}

			this.onRowClick.emit(row);

			if (row[INLINE_EDIT_ENABLED]) {
				const isTdElement = e.target.localName.toLowerCase() === 'td';

				if (isTdElement) {
					this._inlineInputParentWidth = e.srcElement.offsetWidth;
					this._inlineInputWidth = e.srcElement.offsetWidth;
				} else if (e.target.id === 'TTTTTTTTTT') {
					this._inlineInputParentWidth = e.target.offsetParent.offsetWidth;
					this._inlineInputWidth = e.target.offsetParent.offsetWidth;
				} else {
					let canReturn = false;
					let elCancel = document.getElementById('inlineCancel');
					if (elCancel) {
						let rect = elCancel.getBoundingClientRect();
						let location = { x: e.clientX, y: e.clientY };
						if (
							location.x > rect.left &&
							location.x < rect.right &&
							location.y > rect.top &&
							location.y < rect.bottom
						) {
							this._onInlineEditCanceled(e, col, row);
							canReturn = true;
						}
					}
					let elOk = document.getElementById('inlineOk');
					if (elOk) {
						let rect = elOk.getBoundingClientRect();
						let location = { x: e.clientX, y: e.clientY };
						if (
							location.x > rect.left &&
							location.x < rect.right &&
							location.y > rect.top &&
							location.y < rect.bottom
						) {
							this._onInlineEditEnterPressed(e, col, row);
							canReturn = true;
						}
					}
					this.setInlineWidth(e);
					if (canReturn) {
						return;
					}
				}

				setTimeout(() => {
					let el = document.getElementById('inlineEdit');
					if (el) {
						el.focus();
					}
				}, 50);
			}
		}
	}

	private setInlineWidth(e: any) {
		let ttelem = document.querySelectorAll('[id=TTTTTTTTTT]');
		ttelem.forEach((x: HTMLElement) => {
			let rect = x.getBoundingClientRect();
			if (e.clientX > rect.left && e.clientX < rect.right && e.clientY > rect.top && e.clientY < rect.bottom) {
				let _parent = x.offsetParent as HTMLElement;
				this._inlineInputParentWidth = _parent.offsetWidth;
				this._inlineInputWidth = _parent.offsetWidth;
			}
		});
	}

	public _onRowDblClick(event, col: UiTableColumn, row: T) {
		if (!col.registerClick) {
			return;
		}
		this.isSingleClick = false;

		// Reset inline edit enabled mode
		this.uiTableConfig.dataSource.data.forEach((r) => {
			r[INLINE_EDIT_ENABLED] = false;
			r[TABLE_IS_SELECTED] = false;
		});

		let inlineEnabled = !_.isNil(col.inlineEditEnabled) && col.inlineEditEnabled(row);

		// enable inline editing for TableTree child row
		if (this.uiTableConfig.tableTree.enabled && !_.isNil(col.childColumn)) {
			inlineEnabled = !_.isNil(col.childColumn?.inlineEditEnabled) && col.childColumn.inlineEditEnabled(row);
		}

		row[TABLE_IS_SELECTED] = true;
		row[INLINE_EDIT_ENABLED] = inlineEnabled;

		this.uiTableConfig.columns.forEach((x) => {
			x.showInline = x.id === col.id;
		});

		if (row[INLINE_EDIT_ENABLED]) {
			const isTdElement = event.target.localName.toLowerCase() === 'td';
			const isInlineEditIcon =
				event.target.localName.toLowerCase() === 'svg' || event.target.localName.toLowerCase() === 'path';

			if (isTdElement) {
				this._inlineInputParentWidth = event.srcElement.offsetWidth;
				this._inlineInputWidth = event.srcElement.offsetWidth;
			} else if (isInlineEditIcon) {
				const parentDiv = event.path.find((x) => {
					if (x.id === 'TTTTTTTTTT') {
						return x;
					}
				});
				this._inlineInputParentWidth = parentDiv.offsetParent.offsetWidth;
				this._inlineInputWidth = parentDiv.offsetWidth;
			} else {
				this._inlineInputParentWidth = event.target.offsetParent.offsetWidth;
				this._inlineInputWidth = event.target.offsetParent.offsetWidth;
			}

			setTimeout(() => {
				let el = document.getElementById('inlineEdit');
				if (el) {
					el.focus();
				}
			}, 50);
		}

		this.onRowDoubleClick.emit(row);
	}
	public _onCellDropdownChanged(e: MatSelectChange, row: T, column: UiTableColumn) {
		let res = new UiTableCellDropdownChanged();
		res.value = e.value;
		res.row = row;

		this.uiTableConfig.columns.forEach((x) => {
			if (x.id === column.id) {
				row[x.fieldName] = e.value;
			}
		});

		this.onCellDropdownChanged.emit(res);
	}
	public _onCellDblClick(event, col: UiTableColumn, row: T) {
		let args = new UiTableCellClicked(null, row, col);
		this.onCellDblClicked.emit(args);
	}
	public _onCellClicked(row: T, column: UiTableColumn) {
		// router link is set for this cell
		if (!_.isNil(column.routerLink)) {
			const routerLink = column.routerLink(row);
			let args = new UiTableCellClicked(routerLink, row, column);
			this.onCellClicked.emit(args);
		}
	}
	public isCellRouterLink(column: UiTableColumn) {
		return !_.isNil(column.routerLink);
	}

	public _onInlineEditFinished(e, col: UiTableColumn, row: T) {
		console.log('oninlineeditfinished');

		let newValue = e.target.value;

		this._handleInlineEditFinished(e, col, row, newValue);
	}

	isInside(rect) {
		let location;
		return location.x > rect.left && location.x < rect.right && location.y > rect.top && location.y < rect.bottom;
	}

	_onInlineEditEnterPressed(e, col: UiTableColumn, row: T) {
		let canHide = false;
		let canSave = true;

		let elCancel = document.getElementById('inlineCancel');
		if (elCancel) {
			let rect = elCancel.getBoundingClientRect();
			let location = this._mouseLocation;

			if (
				location.x > rect.left &&
				location.x < rect.right &&
				location.y > rect.top &&
				location.y < rect.bottom
			) {
				this._onInlineEditCanceled(e, col, row);
				canHide = true;
				canSave = false;
			}
		}
		let elOk = document.getElementById('inlineOk');
		if (elOk) {
			let rect = elOk.getBoundingClientRect();
			let location = this._mouseLocation;

			if (
				location.x > rect.left &&
				location.x < rect.right &&
				location.y > rect.top &&
				location.y < rect.bottom
			) {
				this._onInlineEditFinished(e, col, row);
				canHide = true;
				canSave = false;
			}
		}

		if (canSave) {
			canHide = true;
			this._onInlineEditFinished(e, col, row);
		}

		if (canHide) {
			row[INLINE_EDIT_ENABLED] = false;
			this.uiTableConfig.columns.forEach((x) => {
				x.showInline = false;
			});
		}
	}

	_onInlineEdit_Paste(event: ClipboardEvent, column: UiTableColumn) {
		console.log(event);
		let clipboardData = event.clipboardData;
		let pastedText = clipboardData.getData('text');

		if (column.inlineEditStringLimit) {
			if (column.inlineEditStringLimit < pastedText.length) {
				// (+) Cut anything that goes above the limit
				column.cellText = function placeholder(params: any) {
					return pastedText.substring(0, column.inlineEditStringLimit);
				};
			}
		}
	}

	_onInlineEdit_KeyDown(event: KeyboardEvent, column: UiTableColumn) {
		// Allow Backspace, tab, end, and home keys
		let el: any = event.target;

		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		let current: string = el.value;
		const position = el.selectionStart;

		let next: string = [
			current.slice(0, position),
			event.key == 'Decimal' ? '.' : event.key,
			current.slice(position),
		].join('');

		if (column.inlineEditStringLimit) {
			if (column.inlineEditStringLimit < next.length) {
				// (+) Cut anything that goes above the limit
				next = next.substring(0, column.inlineEditStringLimit);
				event.preventDefault();
			}
		}

		if (column.cellDataType === UiTableCellDataType.Number) {
			if (next && !String(next).match(this.regex)) {
				event.preventDefault();
			}
		}
	}

	_onInlineEditCanceled(e, col: UiTableColumn, row: T) {
		row[INLINE_EDIT_ENABLED] = false;
		this.uiTableConfig.columns.forEach((x) => {
			x.showInline = false;
		});
		this._handleInlineEditFinished(e, col, row, null);
	}

	_handleInlineEditFinished(e, col, row, newValue) {
		const isParent = this.isParentRow(row);

		// handle decimal point localization setting
		if (col.cellDataType === UiTableCellDataType.Number) {
			if (newValue !== '') {
				newValue = parseFloat(newValue.replace(',', '.')).toFixed(2);
			} else {
				newValue = '0';
			}
			e.target.value = newValue;
		}

		row[INLINE_EDIT_ENABLED] = false;
		this.uiTableConfig.columns.forEach((x) => {
			x.showInline = false;
		});

		if (isParent && !_.isNil(col.onInlineEditFinished)) {
			col.onInlineEditFinished(newValue, row);
		} else if (!isParent && !_.isNil(col.childColumn?.onInlineEditFinished)) {
			const ret = row[TABLE_ROW_ORIG] as K;
			col.childColumn.onInlineEditFinished(newValue, row, ret);
		}
	}

	getInlineInputType(col: UiTableColumn) {
		// if (col.cellDataType === UiTableCellDataType.Number) {
		// 	return 'text';
		// }
		return 'text';
	}
	public _onExportCSV() {
		let csvContent = this.convertRowsToCsv();
		var blob = new Blob([csvContent], { type: 'text/csv'.toString() });
		saveAs(blob, 'tableData.csv');
	}
	_onSortingChanged(event: Sort) {
		this.onSortingChanged.emit(event);
	}
	_onSort(sort: Sort) {
		this.uiTableConfig.dataSource.sort.sort(<MatSortable>{ id: sort.active, start: sort.direction });
	}
	//#endregion

	//#region ========== CHECKBOX SELECTION ==========

	public checkboxSelection = new SelectionModel<T>(true, []);

	public _onToggleChilds(e, row) {
		const rowId = row[TABLE_ROWID];
		const parentIsExpanded = row[TABLE_ROW_ISEXPANDED];
		row[TABLE_ROW_ISEXPANDED] = !parentIsExpanded;

		this.uiTableConfig.dataSource.data.forEach((x) => {
			const rowChildRefId = x[TABLE_ROWID_PARENT];
			if (rowChildRefId === rowId) {
				x[TABLE_ROW_ISVISIBLE] = row[TABLE_ROW_ISEXPANDED];
			}
		});
		e.stopPropagation();
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.checkboxSelection.selected.length;
		const numRows = this.uiTableConfig.dataSource.data.length;
		return numSelected === numRows;
	}

	isAllFilteredSelected() {
		const numSelected = this.checkboxSelection.selected.length;
		const numRows = this.uiTableConfig.dataSource.filteredData.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected() || this.isAllFilteredSelected()
			? this.checkboxSelection.selected.forEach((row) => {
					if (!row[TABLE_ROW_ISDISABLED]) {
						this.checkboxSelection.toggle(row);
					}
			  })
			: !_.isNil(this.activeFilters) && this.activeFilters.length > 0
			? this.uiTableConfig.dataSource.filteredData.forEach((row) => {
					if (!row[TABLE_ROW_ISDISABLED]) {
						this.checkboxSelection.select(row);
					}
			  })
			: this.uiTableConfig.dataSource.data.forEach((row) => {
					if (!row[TABLE_ROW_ISDISABLED]) {
						this.checkboxSelection.select(row);
					}
			  });

		const selectedRows = this.checkboxSelection.selected.map((x) => <T>x);
		this.uiTableConfig.checkboxSelectedRows = selectedRows;
		this.onCheckboxSelectionChanged.emit(selectedRows);
	}

	_onSelectCheckboxRow(e, row) {
		if (row[TABLE_ROW_ISDISABLED]) {
			return;
		}
		if (this.uiTableConfig.table?.enableRowCheckbox) {
			this._onCheckboxSelectionChanged(e, row);
		}
	}
	_onCheckboxSelectionChanged(e, row) {
		if (!this.uiTableConfig.checkbox.multiselect) {
			// multiselset = false
			// 1. odznači vse obkljukane
			// 2. obkljukat trenutno izbranega
			const selectedRows = this.checkboxSelection.selected.map((x) => <T>x);
			this.uiTableConfig.checkboxSelectedRows = selectedRows;

			selectedRows.forEach((element) => {
				this.checkboxSelection.toggle(element);
			});
		}
		const isChecked = e.checked;
		this.checkboxSelection.toggle(row);

		if (!e.checked) {
			this.checkboxSelection.deselect(row);
		}
		// get all child rows and add then to selected
		if (row[TABLE_ROW_ISPARENT]) {
			const parentId = row[TABLE_ROWID];
			let childsToToggle = this.uiTableConfig.dataSource.data.filter((x) => {
				if (x[TABLE_ROWID_PARENT] === parentId && !x[TABLE_ROW_ISDISABLED]) {
					return x;
				}
			});

			if (isChecked) {
				this.checkboxSelection.select(...childsToToggle);
			} else {
				this.checkboxSelection.deselect(...childsToToggle);
			}
		}

		const selectedRows = this.checkboxSelection.selected.map((x) => <T>x);

		this.uiTableConfig.checkboxSelectedRows = selectedRows;
		this.onCheckboxSelectionChanged.emit(selectedRows);
	}
	//#endregion

	//#region ========== INCLUDING/EXCLUDING ALL ==========
	selectionChanged(e, column: UiTableColumn) {
		let item = new UiTableExcludeOrIncludeChanged();
		item.column = column;
		item.value = e.checked;
		this.onIncludeOrExludeSelectionChanged.emit(item);
	}
	//#endregion

	//#region ========== FILTERING ==========

	showFilter(column: UiTableColumn) {
		this.initFilter();
		this.templateFor = column.fieldName;
	}
	clearFilter(field: string) {
		const filterIndex = this.activeFilters.findIndex((e: UiTableFilterArgs) => {
			if (e.field === field) {
				return e;
			}
		});
		if (filterIndex !== -1) {
			this.activeFilters.splice(filterIndex, 1);
		}
		this.uiTableConfig.dataSource.filter = JSON.stringify(this.activeFilters);
		this.uiTableConfig.table.activeFilters = this.mapActiveFilters(this.activeFilters);
		this.onClearFilter.next(field);
	}

	mapActiveFilters(filters: UiTableFilterArgs[]): UiTableFilters {
		let tmp = new UiTableFilters();
		tmp.tableId = this.uiTableConfig.table.ident;
		tmp.filters = JSON.stringify(filters);
		return tmp;
	}
	initFilter() {
		this.uiTableConfig.dataSource.filterPredicate = (data: T, filter: string) => {
			const filtersObj: UiTableFilterArgs[] = JSON.parse(filter);
			let filterRes = true;
			filtersObj.forEach((x: UiTableFilterArgs) => {
				if (!filterRes) {
					return false;
				}

				if (x.filterDataType === UiTableCellDataType.Text) {
					filterRes = this._searchText(data, x);
				} else if (x.filterDataType === UiTableCellDataType.Number) {
					filterRes = this._searchNumber(data, x);
				} else if (x.filterDataType === UiTableCellDataType.Dropdown) {
					filterRes = this._searchDropdown(data, x);
				} else if (x.filterDataType === UiTableCellDataType.Boolean) {
					filterRes = this._searchBool(data, x);
				}
			});
			this.uiTableConfig.table.activeFilters = this.mapActiveFilters(this.activeFilters);
			return filterRes;
		};
	}
	_searchText(data: T, x: UiTableFilterArgs): boolean {
		let cellObj = data[x.field];
		const cellObjType = typeof cellObj;
		let cellValue: string | number = '';

		// if cell value is object, get cell value from object of type {Id,Value}
		if (cellObjType === 'object' && !_.isNil(cellObj)) {
			cellValue = cellObj['Value']; // if cell has Id/Value pair, read text from Value
		} else {
			cellValue = cellObj; // cellObj is plain text
		}

		let cellValueFinal = '';
		if (cellObjType === 'number') {
			cellValueFinal = (cellValue as number).toFixed(5).toString();
		} else {
			// if cell has not value, set it to empty string
			if (_.isNil(cellValue)) {
				cellValue = '';
			}
			cellValueFinal = (cellValue as string).toLowerCase();
		}

		const res = cellValueFinal.indexOf(x.filterText?.toLowerCase()) !== -1;
		return res;
	}
	_searchNumber(data: T, x: UiTableFilterArgs): boolean {
		return true;
	}
	_searchDropdown(data: T, x: UiTableFilterArgs): boolean {
		const cellObj = data[x.field];

		if (_.isNil(cellObj)) {
			return false;
		}

		const cellValueFromObj = cellObj['Value']; // if cell has Id/Value pair, read text from Value

		let textToSearch = cellObj; // default binding
		if (cellValueFromObj) {
			textToSearch = cellValueFromObj;
		}
		if (!textToSearch) {
			textToSearch = '';
		}
		textToSearch = textToSearch.toLowerCase();
		const res = textToSearch === x.filterDropdownSelected.toLowerCase();
		return res;
	}
	_searchBool(data: T, x: UiTableFilterArgs): boolean {
		const cellValue = data[x.field];
		return cellValue === x.filterBool;
	}
	_searchDate(data: T, x: UiTableFilterArgs): boolean {
		return false;
	}
	_searchDateRange(data: T, x: UiTableFilterArgs): boolean {
		return false;
	}

	// Get active filter by column fieldName of exists.
	getActiveFilter(field: string): UiTableFilterArgs {
		return this.activeFilters.find((e: UiTableFilterArgs) => {
			if (e.field === field) {
				return e;
			}
		});
	}
	getOrCreateFilterData(column: UiTableColumn) {
		let existingFilter = this.getActiveFilter(column.fieldName);

		let dropdownValues: string[] = [];
		if (column.filterDataType === UiTableCellDataType.Dropdown) {
			this.uiTableConfig.dataSource.data.forEach((x) => {
				const val = x[column.fieldName];
				if (dropdownValues.findIndex((x) => x === val) === -1) {
					if (!_.isNil(val) && val !== '') {
						dropdownValues.push(val);
					}
				}
			});
		}

		if (existingFilter) {
			existingFilter.filterDropdown = dropdownValues;
			return existingFilter;
		}

		let newFilter = new UiTableFilterArgs();
		newFilter.field = column.fieldName;
		newFilter.fieldFilter = column.fieldNameFilter;
		newFilter.filterDataType = column.filterDataType;
		newFilter.filterDropdown = dropdownValues;
		return newFilter;
	}

	_filterClosed() {
		this.templateFor = null;
	}
	_filterChanged(filter: UiTableFilterArgs) {
		// check if current filter is already active
		const activeFilter = this.getActiveFilter(filter.field);

		if (activeFilter) {
			activeFilter.filterText = filter.filterText;
			activeFilter.filterBool = filter.filterBool;
		} else {
			this.activeFilters.push(filter);
		}

		this.uiTableConfig.dataSource.filter = JSON.stringify(this.activeFilters);
		this.uiTableConfig.table.activeFilters = this.mapActiveFilters(this.activeFilters);

		if (this.uiTableConfig.dataSource.paginator) {
			this.uiTableConfig.dataSource.paginator.firstPage();
		}
	}
	_filterCleared(filter: UiTableFilterArgs) {
		this.clearFilter(filter.field);
	}
	_filterEscaped() {
		this.templateFor = null;
	}

	_reApplyFilters() {
		if (this.activeFilters && this.activeFilters.length > 0) {
			this.initFilter();
			this.uiTableConfig.dataSource.filter = JSON.stringify(this.activeFilters);
			this.uiTableConfig.table.activeFilters = this.mapActiveFilters(this.activeFilters);
			// setTimeout(() => {
			// this._filterChanged(this.activeFilters[0]);
			// }, 1);
		}
	}

	importActiveFilters(data: UiTableFilters) {
		if (!_.isNil(data)) {
			this.activeFilters = JSON.parse(data.filters);
			this._reApplyFilters();
		}
	}

	//#endregion

	//#region ========== STYLING ==========
	getTableContainerStyle() {
		let style = {
			maxHeight: '',
			overflowX: '',
			overflowY: '',
			position: '',
		};

		if (!_.isNil(this.uiTableConfig.table.maxHeightPx))
			style.maxHeight = `${this.uiTableConfig.table.maxHeightPx}px`;

		if (!_.isNil(this.tableOverflowX)) {
			style.overflowX = this.tableOverflowX;
		}
		if (!_.isNil(this.tableOverflowY)) {
			style.overflowY = this.tableOverflowY;
		}
		if (this.uiTableConfig.table.isExportVisible) {
			style.position = 'relative';
		}
		return style;
	}

	public getTableStyle() {
		let style = {
			width: '100%',
			border: 'none',
		};

		if (this.uiTableConfig.table.width) {
			style.width = this.uiTableConfig.table.width;
		}
		if (this.uiShowBorder) {
			if (this.uiBlackBorder) {
				style.border = `1px solid #455053 !important`;
			} else {
				style.border = `1px solid #c8c8c8 !important`;
			}
		}

		return style;
	}
	public getTableHeaderClass() {
		let classes = '';

		if (!this.uiTableConfig.header.showHeader) {
			classes = 'table-header-hide ';
		}
		if (this.uiTableConfig.header.cssClassHeaderRow) {
			classes += this.uiTableConfig.header.cssClassHeaderRow;
		}
		return classes;
	}
	public getHeaderCellClass(column: UiTableColumn) {
		let classes = '';

		if (this.uiTableConfig.header.cssClassHeaderCell) {
			classes = this.uiTableConfig.header.cssClassHeaderCell;
		}
		if (!column.showOnMobile) {
			classes += ' hide-cell-on-phone';
		}
		return classes;
	}
	public getHeaderCellStyle(column: UiTableColumn) {
		//checkif width in px or %
		let style = {
			textAlign: '',
			width: '',
		};

		if (column.minimumSize) {
			style.width = '1px';
		}

		if (column.width) {
			style.width = column.width;
		}

		if (column.cellDataType === UiTableCellDataType.Number) {
			style.textAlign = 'end';
		} else if (column.cellDataType === UiTableCellDataType.Boolean) {
			style.textAlign = 'center';
		}

		return style;
	}

	public getCheckboxColumnStyle() {
		let style = {
			flex: `0 0 21px`,
		};
		return style;
	}

	public isCheckboxDisabled(row) {
		return row[TABLE_ROW_ISDISABLED];
	}

	public getRowStyle(row) {
		let style = {
			display: '',
			color: '',
			background: '',
		};
		if (_.has(row, TABLE_ROW_ISCHILD) && row[TABLE_ROW_ISCHILD] && !row[TABLE_ROW_ISVISIBLE]) {
			style.display = 'none';
		}

		if (row[TABLE_ROW_ISDISABLED]) {
			style.background = '#c4c4c4';
		}

		return style;
	}

	public getCellClass(column: UiTableColumn, row) {
		let classes = '';

		if (column.cellIcons) {
			classes = 'table-cell-icon';
		}
		if (!column.showOnMobile) {
			classes += ' hide-cell-on-phone';
		}

		classes += ' ' + this.getCellContentAligment(column);

		if (this.shouldHighlightCellOnHover(column, row)) {
			classes += ' mat-row-hover';
		}

		if (column.cellDisabled) {
			classes += 'disabled-cell';
		}
		return classes;
	}
	public getCellStyle(column: UiTableColumn, row: T) {
		//checkif width in px or %
		let style = {
			justifyContent: '',
			width: '',
			maxWidth: '',
			display: 'flex',
		};

		// if (column.minimumSize) {
		// 	style.width = '1px';
		// }

		if (column.width) {
			style.width = column.width;
		}

		if (column.cellContentAlign === UiTableCellContentAlign.Center) {
			style.justifyContent = 'center';
		} else if (column.cellContentAlign === UiTableCellContentAlign.Right) {
			style.justifyContent = 'flex-end';
		}

		if (column.cellDataType === UiTableCellDataType.Number) {
			style.justifyContent = 'flex-end';
		} else if (column.cellDataType === UiTableCellDataType.Boolean) {
			style.justifyContent = 'center';
		}

		if (
			column.cellDataType === UiTableCellDataType.Date ||
			column.cellDataType === UiTableCellDataType.DateTime ||
			column.cellDataType === UiTableCellDataType.Dropdown
		) {
			style.display = '';
		}

		return style;
	}
	public getCellTdStyle(column: UiTableColumn, row: T) {
		//checkif width in px or %
		let style = {
			width: '',
			maxWidth: '',
		};

		if (
			// _.has(row, INLINE_EDIT_ENABLED) &&
			row[INLINE_EDIT_ENABLED] &&
			column.showInline &&
			(this.isParentInlineEditEnabled(row, column) || this.isChildInlineEditEnabled(row, column))
			// &&
			// ((column.inlineEditEnabled && column.inlineEditEnabled(row)) ||
			// 	(column.childColumn?.inlineEditEnabled && column.childColumn?.inlineEditEnabled(row)))
		) {
			// style.width = `${this._inlineInputParentWidth - 11}px`;
			// style.maxWidth = `${this._inlineInputParentWidth - 11}px`;

			let width = this._inlineInputParentWidth;
			style.width = `${width}px`;
			style.maxWidth = `${width}px`;
		}

		return style;
	}
	public getCellDivStyle(row: T, col: UiTableColumn, isInputCtrl: boolean = false, isDiv: boolean = false) {
		let style = this.getCellStyle(col, row);

		return style;
	}
	public getInlineInputStyle(row: T, col: UiTableColumn) {
		let style = {
			width: '',
			maxWidth: '',
			minWidth: '',
		};

		// inline input hidden
		if (!row[INLINE_EDIT_ENABLED] || (!col.showInline && !col.inlineEditEnabled)) {
			return style;
		}

		// inline input visible
		if (col.showInline) {
			let width =
				col.cellDataType === UiTableCellDataType.Number
					? this._inlineInputWidth - 19
					: this._inlineInputWidth - 12;

			// let width = this._inlineInputWidth;
			style.width = `${width}px`;
			style.maxWidth = `${width}px`;

			if (col.inlineEditShowSideButtons) {
				style.minWidth = `${width}px`;
			}
		}

		return style;
	}
	public getIconStyle(icon: UiTableCellIconConfiguration, row) {
		if (!_.isNil(icon.getIconStyle)) {
			return icon.getIconStyle(row);
		} else {
			let style = {
				cursor: '',
				backgroundColor: '',
			};
			if (icon.isRouterLink) {
				style.cursor = 'pointer';
			}
			return style;
		}
	}

	public getInlineStylePaginator() {
		let style = {
			width: '',
			border: 'none',
			display: 'block',
		};
		if (this.uiTableConfig.table.width) {
			style.width = this.uiTableConfig.table.width;
		}
		if (!this.uiTableConfig.paginator.showPaginator) {
			style.display = 'none';
		}
		return style;
	}

	public getTreeTableToggleIcon(row) {
		return row[TABLE_ROW_ISEXPANDED] ? this.iconSvc.falChevronUp : this.iconSvc.falChevronDown;
	}
	public formatChildIndentation(column: UiTableColumn, row, isFirstCol: boolean) {
		if (_.has(row, TABLE_ROW_ISCHILD) && row[TABLE_ROW_ISCHILD] && isFirstCol) {
			const res = '&nbsp;'.repeat(10);
			return res;
		}
		return '';
	}

	public getIconContainerStyle(column: UiTableColumn) {
		var style = {
			justifyContent: 'center',
			color: '',
			flex: '1',
		};

		if (column.cellIconAlign === UiTableIconAlign.Center) {
			style.justifyContent = 'center';
		} else if (column.cellIconAlign === UiTableIconAlign.Spread) {
			style.justifyContent = 'space-between';
		} else if (column.cellIconAlign === UiTableIconAlign.Left) {
			style.justifyContent = 'flex-start';
		} else if (column.cellIconAlign === UiTableIconAlign.Right) {
			style.justifyContent = 'flex-end';
		}

		if (column.cellIconMinSize) {
			style.flex = '';
		}

		return style;
	}

	public getCellContentAligment(col: UiTableColumn) {
		switch (col.cellContentAlign) {
			case UiTableCellContentAlign.Center:
				return 'mat-cell-center';
			case UiTableCellContentAlign.Right:
				return 'mat-cell-right';
			case UiTableCellContentAlign.Left:
				return 'mat-cell-left';
			default:
				return 'mat-cell-left';
		}
	}

	public setFilterIconColor(field: string) {
		if (this.getActiveFilter(field)) {
			return 'icon-red';
		}
	}

	//#endregion

	//#region ========== HELPERS ==========
	private highlightRow() {}

	private isParentRow(row) {
		return _.has(row, TABLE_ROW_ISPARENT) && row[TABLE_ROW_ISPARENT];
	}
	private isChildRow(row) {
		return _.has(row, TABLE_ROW_ISCHILD) && row[TABLE_ROW_ISCHILD];
	}

	public getCellIconsArray(col: UiTableColumn): UiTableCellIconConfiguration[] {
		//take parent icon configuration by default
		let iconConf = col.cellIcons;

		// return child icon configuration
		if (!_.isNil(col.childColumn?.cellIcons)) {
			iconConf = col.childColumn.cellIcons;
		}
		return iconConf;
	}

	isCellEditable(column: UiTableColumn, row): boolean {
		if (!this.isChildRow(row)) {
			return !_.isNil(column?.inlineEditEnabled) && column.inlineEditEnabled(row);
		}
		return !_.isNil(column?.childColumn?.inlineEditEnabled) && column.childColumn.inlineEditEnabled(row);
	}
	showInlineEdit(column: UiTableColumn, row): boolean {
		// set inline for flat (single level) table
		let inline = row[INLINE_EDIT_ENABLED] && !_.isNil(column?.inlineEditEnabled) && column.inlineEditEnabled(row);
		inline = inline && column.showInline;

		// set inline for multi level table (parent-child)
		if (
			row[INLINE_EDIT_ENABLED] &&
			column.showInline &&
			this.isChildRow(row) &&
			!_.isNil(column.childColumn.inlineEditEnabled)
		) {
			inline = column.childColumn.inlineEditEnabled(row);
		}
		return inline;
	}

	private isParentInlineEditEnabled(row: T, column: UiTableColumn) {
		if (!this.isParentRow(row)) {
			return false;
		}
		return column.inlineEditEnabled && column.inlineEditEnabled(row);
	}
	private isChildInlineEditEnabled(row: T, column: UiTableColumn) {
		if (this.isParentRow(row)) {
			return false;
		}
		return column.childColumn?.inlineEditEnabled && column.childColumn?.inlineEditEnabled(row);
	}

	private convertRowsToCsv() {
		if (!this.uiTableConfig.dataSource.data || !this.uiTableConfig.dataSource.data.length) {
			return;
		}
		const separator = ';';
		const keys = Object.keys(this.uiTableConfig.dataSource.data[0]);
		let csvContent = '';
		let csvHeader = '';

		let rowCount = 0;
		this.uiTableConfig.dataSource.data.forEach((r) => {
			keys.forEach((k) => {
				const tableColumn = this.uiTableConfig.columns.find((x) => x.fieldName === k);
				if (tableColumn && tableColumn.isVisible && tableColumn.cellDataType !== UiTableCellDataType.Icon) {
					if (rowCount === 0) {
						csvHeader += `${tableColumn.headerText};`;
					}
					let text: string = '';
					if (isNumber(r[k])) {
						text = r[k].toString();
					} else {
						text = r[k];
					}
					if (text && (text.indexOf('↵') > -1 || text.indexOf('\n') > -1)) {
						text = text.replace('↵', '').replace(/\r?\t?\n?/g, '');
						text.trim();
					}
					csvContent += `${text};`;
				}
			});
			if (rowCount === 0) {
				csvHeader += '\n';
			}
			csvContent += '\n';
			rowCount++;
		});

		return csvHeader + csvContent;
	}

	getCellText(row: T, col: UiTableColumn) {
		const id = `${row[TABLE_ROWID]}-${col.id}}`;

		// Check if current cell is already cached
		const res = this.CACHE.getCachedCell(col, row);
		if (res) {
			return res.value;
		}

		// Current cell value not cached yet

		// Multilevel table - Child row
		//console.log(col.childColumn.cellText(row[TABLE_ROW_ORIG]));
		let text = '';
		if (this.uiTableConfig.tableTree.enabled && this.isChildRow(row) && !_.isNil(col.childColumn)) {
			// Custom text

			if (!_.isNil(col.childColumn.cellText)) {
				text = col.childColumn.cellText(row[TABLE_ROW_ORIG]);

				if (col.childColumn.cellDataType === UiTableCellDataType.Number && text) {
					text = this.formatCellNumber(text, col.childColumn.cellNumOfDigits);
				}
			}
			// Text from fieldName
			else {
				let text = row[TABLE_ROW_ORIG][col.childColumn.fieldName];
				if (col.childColumn.cellDataType === UiTableCellDataType.Number && text) {
					text = this.formatCellNumber(text, col.childColumn.cellNumOfDigits);
				}
			}
		}
		// Flat table - Parent row
		else {
			if (!_.isNil(col.cellText)) {
				text = col.cellText(row);
			} else {
				text = row[col.fieldName];
			}
			if (col.cellDataType === UiTableCellDataType.Number && text) {
				text = this.formatCellNumber(text, col.cellNumOfDigits);
			}
		}
		this.CACHE.addCacheEntry(id, text);
		return text;
	}

	getCellTooltip(column: UiTableColumn, row: T) {
		// return 'tooltip text';
		// no tooltip method defined
		if (_.isNil(column.getTooltipText)) {
			return '';
		}

		// show tooltip
		return column.getTooltipText(row);
	}

	private formatCellNumber(text: any, numOfDigits): string {
		const digits = _.isNil(numOfDigits) ? this.textSvc.defaultDigits : numOfDigits;
		text = (<number>text).toFixed(digits).replace('.', this.textSvc.digitsSeparator);
		return text;
	}
	//#endregion
}
