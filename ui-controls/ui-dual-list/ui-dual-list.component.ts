import { Component, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import {
	UiDualListConfig,
	UiDualListEvent,
	UiDualListEventType,
	UiTableConiguration,
} from '../../entities/ui-controls';
import { UiIconService } from './../../services/ui-icon.service';
import { UiTextService } from './../../services/ui-text.service';

@Component({
	selector: 'ui-dual-list',
	templateUrl: './ui-dual-list.component.html',
	styleUrls: ['./ui-dual-list.component.scss'],
	host: { class: 'ui-dual-list' },
})
export class UiDualListComponent<T> implements OnInit {
	@Input() leftTableConfig: UiTableConiguration<T>;
	@Input() rightTableConfig: UiTableConiguration<T>;
	@Input() leftTitle: string;
	@Input() rightTitle: string;
	@Input() actionsTitle: string;
	@Input() uiDualConfig: UiDualListConfig;

	@Input() showMoveAllLeft: boolean = true;
	@Input() showMoveAllRight: boolean = true;

	@Output() eventInfo: Subject<UiDualListEvent>;

	@Input() showTitle: boolean = false;

	constructor(public iconSvc: UiIconService, public textSvc: UiTextService) {
		if (this.uiDualConfig == null) {
			this.uiDualConfig = new UiDualListConfig();
		}

		this.eventInfo = new Subject();
	}

	ngOnInit(): void {}

	/** Moves all from left to right */
	moveAllRight() {
		const data = Array.from(this.leftTableConfig.dataSource.data);
		this.moveItems(this.leftTableConfig.dataSource.data, this.rightTableConfig.dataSource.data, null, true);

		this.eventInfo.next(new UiDualListEvent(UiDualListEventType.MOVED_ALL_RIGHT, 'All items moved right', data));
	}

	/** Moves all from right to left */
	moveAllLeft() {
		const data = Array.from(this.rightTableConfig.dataSource.data);
		this.moveItems(this.rightTableConfig.dataSource.data, this.leftTableConfig.dataSource.data, null, false);

		this.eventInfo.next(new UiDualListEvent(UiDualListEventType.MOVED_ALL_LEFT, 'All items moved left', data));
	}

	/** Move selected items from left to right */
	moveSelectedRight() {
		const data = Array.from(this.leftTableConfig.checkboxSelectedRows ?? []);
		this.moveItems(
			this.leftTableConfig.dataSource.data,
			this.rightTableConfig.dataSource.data,
			this.leftTableConfig.checkboxSelectedRows ?? [],
			true
		);

		this.eventInfo.next(new UiDualListEvent(UiDualListEventType.MOVED_RIGHT, 'Selected items moved right.', data));
	}

	/** Move selected items from right to left */
	moveSelectedLeft() {
		const data = Array.from(this.rightTableConfig.checkboxSelectedRows ?? []);
		this.moveItems(
			this.rightTableConfig.dataSource.data,
			this.leftTableConfig.dataSource.data,
			this.rightTableConfig.checkboxSelectedRows ?? [],
			false
		);

		this.eventInfo.next(new UiDualListEvent(UiDualListEventType.MOVED_LEFT, 'Selected items moved left.', data));
	}

	/** Main move logic */
	moveItems(from: T[], to: T[], items: T[], leftToRight: boolean) {
		if (items == null) {
			for (let i = from.length - 1; i >= 0; i--) {
				let poped = from.pop();
				to.push(poped);
			}
		} else {
			items?.forEach((item) => {
				const index = from.indexOf(item);
				if (index >= 0) {
					from.splice(index, 1);
					to.push(item);
				}
			});
		}

		this.leftTableConfig.initDataSource(leftToRight ? from : to);
		this.leftTableConfig.checkbox.clearSelection();
		this.rightTableConfig.initDataSource(leftToRight ? to : from);
		this.rightTableConfig.checkbox.clearSelection();
	}
}
