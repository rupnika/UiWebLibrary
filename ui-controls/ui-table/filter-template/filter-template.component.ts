import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatRadioChange } from '@angular/material/radio';
import { MatSelect } from '@angular/material/select';
import * as _ from 'lodash';
import { UiTableCellDataType } from '../../../entities/enums';
import { UiTableFilterArgs } from '../../../entities/ui-controls';
import { UiIconService } from '../../../services/ui-icon.service';
import { UiTextService } from '../../../services/ui-text.service';
@Component({
	selector: 'ui-filter-template',
	templateUrl: './filter-template.component.html',
	styleUrls: ['./filter-template.component.scss'],
})
export class FilterTemplateComponent implements OnInit {
	myForm: FormGroup;
	@Input() openFilter: boolean;
	@Input() filterHeader: string;
	@Input() data: UiTableFilterArgs;

	/** When filter value changed */
	@Output() filterChanged = new EventEmitter<UiTableFilterArgs>();
	@Output() filterClosed = new EventEmitter();
	@Output() filterCleared = new EventEmitter<UiTableFilterArgs>();
	@Output() filterEscaped = new EventEmitter();

	@ViewChild('filterMenu') triggerMenu: MatMenuTrigger;
	@ViewChild('filterTextInput') filterTextInput: ElementRef;
	dropdownData: string[];

	constructor(public iconSvc: UiIconService, public textSvc: UiTextService) {}

	ngOnInit(): void {
		this.myForm = new FormGroup({
			filter_text: new FormControl(this.data ? this.data.filterText : ''),
			filter_bool: new FormControl(),
			filter_dateFrom: new FormControl(),
			filter_dateTo: new FormControl(),
			filter_dropdown: new FormControl(),
		});

		if (!_.isNil(this.data?.filterDropdownSelected)) {
			this.myForm.patchValue({
				filter_dropdown: this.data.filterDropdownSelected,
			});
		}
	}

	ngAfterViewInit() {
		if (this.openFilter) {
			this.triggerMenu.menuData = this.data;

			this.dropdownData = this.data.filterDropdown;
			this.triggerMenu.openMenu();

			if (this.data.filterDataType === UiTableCellDataType.Text) {
				this.filterTextInput.nativeElement.focus();
			}

			this.triggerMenu.menuClosed.subscribe((_: any) => {
				this.openFilter = false;
				this.filterClosed.emit();
			});
		}
	}

	onFilterChanged(event) {
		// close filter dialog if Esc is pressed
		if (event.code === 'Escape') {
			this.filterEscaped.emit();
		}

		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		filterArgs.filterText = this.myForm.value.filter_text;

		if (filterArgs.filterText === '') {
			this.filterCleared.emit(filterArgs);
		} else {
			this.filterChanged.emit(filterArgs);
		}
	}
	onFilterDropdownChanged(event: MatSelect) {
		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		filterArgs.filterDropdownSelected = event.value;
		this.filterChanged.emit(filterArgs);
	}
	onFilterBoolChanged(event: MatRadioChange) {
		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		filterArgs.filterBool = event.value === '1';
		this.filterChanged.emit(filterArgs);
	}
	onFilterDateFromChanged(event: MatDatepickerInputEvent<Date>) {
		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		filterArgs.filterDateFrom = event.value;
		this.filterChanged.emit(filterArgs);
	}
	onFilterDateToChanged(event: MatDatepickerInputEvent<Date>) {
		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		filterArgs.filterDateTo = event.value;
		this.filterChanged.emit(filterArgs);
	}
	_onClearClick() {
		let filterArgs = <UiTableFilterArgs>this.triggerMenu.menuData;
		this.openFilter = false;
		this.filterClosed.emit();
		this.filterCleared.emit(filterArgs);
	}

	get isFilterText(): boolean {
		return this.data.filterDataType === UiTableCellDataType.Text;
	}
	get isFilterNumber(): boolean {
		return this.data.filterDataType === UiTableCellDataType.Number;
	}
	get isFilterBool(): boolean {
		return this.data.filterDataType === UiTableCellDataType.Boolean;
	}
	get isFilterDate(): boolean {
		return this.data.filterDataType === UiTableCellDataType.Date;
	}
	get isDropdown(): boolean {
		return this.data.filterDataType === UiTableCellDataType.Dropdown;
	}
}
