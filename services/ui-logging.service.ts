import { Injectable } from '@angular/core';
import { HttpStatusCode, LogColor } from '../entities/enums';
import { ApiResponse } from '../entities/misc';

@Injectable({
	providedIn: 'root',
})
export class UiLoggingService {
	constructor() {}

	//#region Logging

	private getDate() {
		return `[${new Date().toISOString()}] `;
	}
	public Log(message: string, color: LogColor, data: any = null) {
		if (data === null) {
			console.log(this.getDate() + '%c' + message, this.GetColor(color));
		} else {
			console.log(this.getDate() + '%c' + message, this.GetColor(color), data);
		}
	}
	public LogRequest(caller, method: string, message: string, data: any) {
		/**
		 * LOGGING
		 */
		if (data === null) {
			console.log(
				this.getDate() + `%c[REQUEST] ${caller.constructor.name}.${method} [ ${message} ]`,
				this.GetColor(LogColor.ServerRequest)
			);
		} else {
			console.log(
				this.getDate() + `%c[REQUEST] ${caller.constructor.name}.${method} [ ${message} ]`,
				this.GetColor(LogColor.ServerRequest),
				data
			);
		}
	}
	public LogResponseAny(caller, method, data: any, message?: any) {
		/**
		 * LOGGING
		 */
		let msg = '';

		if (message) {
			msg = `[ ${message} ]`;
		}
		if (data === null) {
			console.log(
				this.getDate() + `%c[RESPONSE] ${caller.constructor.name}.${method} ${msg}`,
				this.GetColor(LogColor.ServerResponse)
			);
		} else {
			console.log(
				this.getDate() + `%c[RESPONSE] ${caller.constructor.name}.${method} ${msg}`,
				this.GetColor(LogColor.ServerResponse),
				data
			);
		}
	}
	public LogResponse(caller, method, apiResponse: ApiResponse) {
		this.LogResponseAny(caller, method, apiResponse);
	}
	/**
	 * Generic method for logging server response.
	 * @param caller Object that is calling. Usually just current object (this))
	 * @param method Method name
	 * @param apiResponse Server response object
	 */
	public LogApiResponse(caller, method, apiResponse: ApiResponse) {
		if (apiResponse.Status === HttpStatusCode.Ok) {
			this.LogResponse(caller, method, apiResponse);
		} else {
			this.LogResponseError(caller, method, apiResponse);
		}
	}
	public LogResponseError(caller, method: string, apiRes: ApiResponse) {
		console.log(
			this.getDate() +
				'%c[STATUS=' +
				apiRes.Status +
				'] (' +
				caller.constructor.name +
				'.' +
				method +
				') ' +
				apiRes.Message,
			this.GetColor(LogColor.ServerResponseNotOk)
		);
	}
	public LogError(caller, method: string, error: string) {
		console.log(
			this.getDate() + '%c[ERROR] (' + caller.constructor.name + '.' + method + ') ',
			this.GetColor(LogColor.Red)
		);
		console.log(error);
	}

	private GetColor(color: LogColor) {
		let applyColor;

		if (color === LogColor.Red) {
			applyColor = 'color: #D82F1D';
		} else if (color === LogColor.Green || color === LogColor.ServerResponse) {
			applyColor = 'color: #22BA55';
		} else if (color === LogColor.ServerResponseNotOk) {
			applyColor = 'color: #F18D0C';
		} else if (color === LogColor.Blue) {
			applyColor = 'color: #226CBA';
		} else if (color === LogColor.ServerRequest) {
			applyColor = 'color: #AA22BA';
		} else if (color === LogColor.Yellow) {
			applyColor = 'color: #AABB0C';
		}
		return applyColor;
	}

	//#endregion
}
