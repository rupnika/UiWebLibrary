/**
 * dsf
 */
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { Constants } from '../entities/misc';

@Pipe({
	name: 'DateTimePipe',
})
export class UiDateTimePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		return super.transform(value, Constants.dateTimeFormat);
	}
}
@Pipe({
	name: 'DatePipe',
})
export class UiDatePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		return super.transform(value, Constants.dateFormat);
	}
}
@Pipe({
	name: 'TimePipe',
})
export class UiTimePipe extends DatePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		return super.transform(value, Constants.timeFormat);
	}
}
