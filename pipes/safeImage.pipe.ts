import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'safeImage' })
export class UiSafeImagePipe {
	constructor(private sanitizer: DomSanitizer) {}

	transform(base64String) {
		return this.sanitizer.bypassSecurityTrustResourceUrl(base64String);
	}
}
